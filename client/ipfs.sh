npm run build

IPFS_ADDR=$(host ipfs.fluence.one | awk '/has address/ { print $4 }' | head -n+1)

ipfs --api /ip4/$IPFS_ADDR/tcp/5001 add -r build

echo "Please checkout http://ipfs.fluence.one:8080/ipfs/<HASH>"
