This is the client. It is written in a React / Redux stack, and uses the
following library:

* [React](http://reactjs.org)
* [Redux](http://redux.js.org)
* [Redux-thunk](https://github.com/reduxjs/redux-thunk)
* [Fluence](https://fluence.network/)

Usage
-----

`yarn dev` 

Launches a developement server.

`yarn build` 

Build a static version.
