import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import {
  Header,
  Footer,
  LayoutLeftColumn,
  LayoutRightColumn,
  SettingsNavbar
} from "../../components";

import { SignOutPageAttempt } from "../../reducers";
import { Redirect } from "react-router";

class SignOutPage extends Component {
  constructor(props) {
    super();
    this.props = props;
  }

  componentDidMount() {
    this.props.onSignOut();
    return {};
  }

  render() {
    return (
      <Fragment>
        <Header {...this.props.connectedUser} />
        <LayoutLeftColumn>
          <SettingsNavbar current="sign-out" />
        </LayoutLeftColumn>
        <LayoutRightColumn>
          {this.props.signOutSucceed ? (
            <p>
              <strong>
                You successfully signed out! You will be redirected soon.
              </strong>
            </p>
          ) : null}
          {!this.props.connectedUser ? <Redirect to="/" /> : null}
        </LayoutRightColumn>
        <Footer />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  connectedUser: state.connectedUser,
  signOutSucceed: state.signInSucceed
});

const mapDispatchToProps = dispatch => ({
  onSignOut: () => dispatch(SignOutPageAttempt())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignOutPage);
