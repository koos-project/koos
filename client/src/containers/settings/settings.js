import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import {
  Footer,
  Header,
  LayoutLeftColumn,
  LayoutRightColumn,
  NeedSignedIn,
  SettingsNavbar
} from "../../components";

class SettingsPage extends Component {
  constructor(props) {
    super();
    this.props = props;
  }

  componentDidMount() {
    //		this.props.onPageLoad();
    return {};
  }

  render() {
    return (
      <Fragment>
        <NeedSignedIn if={!this.props.connectedUser} to="/" />
        <Header {...this.props.connectedUser} />
        <LayoutLeftColumn>
          <SettingsNavbar current="settings" />
        </LayoutLeftColumn>
        <LayoutRightColumn>
          <button>Delete my profile</button>
        </LayoutRightColumn>

        <Footer />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  connectedUser: state.connectedUser
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsPage);
