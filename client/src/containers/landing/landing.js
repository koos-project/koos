import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import { NeedSignedIn, Footer, HashtagLink, Header } from "../../components";
import RegisterPage from "../register/register";
import { RegisterPageAttempt, PageLoad } from "../../reducers";

class LandingPage extends Component {
  constructor(props) {
    super();
    this.props = props;
  }

  componentDidMount() {
    this.props.onPageLoad();
    return {};
  }

  render() {
    return (
      <Fragment>
        <Header {...this.props.connectedUser} />
        <NeedSignedIn if={this.props.connectedUser} to="/posts" />
        <div className="koos-intro">
          <div className="koss-text-flex koos-flex">
            <div className="intro-text koos-intro-flex">
              <h2>Koos, a social network for the future!</h2>
              <h2>
                Koos is a decentralized, censor-free, trustless social network
                that put trust back in the hand of the people
              </h2>
              <div className="ideals-style">
                <h1 className="landingTitle">Our ideals</h1>
                <div className="ideals-style-flex">
                  <div className="triangleTop ideals-style-text">
                    <h2 className="big">
                      <span className="huge">01</span>Pricacy
                    </h2>
                    <p>
                      Your datas are yours! By design, Koos make it impossible
                      to mine or manipulate what you publish on the network.
                    </p>
                  </div>
                  <div className="triangleLeft ideals-style-text">
                    <h2 className="big">
                      <span className="huge">02</span>Decentralisation
                    </h2>
                    <p>
                      There is no central authority behind Koos. It runs on a
                      peer-to-peer network and is as resilient as the internet
                      itself.
                    </p>
                  </div>
                  <div className="triangleRight ideals-style-text">
                    <h2 className="big">
                      <span className="huge">03</span>Social utility
                    </h2>
                    <p>
                      We're a group of people who believes in radical freedom
                      and independance. We believe in the right to have
                      unmediated, censorship-free communications! Learn more at
                      our <a href="#/project">project</a> page!
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="koos-flex">
            <div className="sign-in-intro koos-intro-flex">
              <RegisterPage standAlone={true} {...this.props} />
              <div className="tryit">
                <p>Or try it!</p>
                <p>
                  <HashtagLink>Permaweb</HashtagLink>
                  <HashtagLink>Blockchain</HashtagLink>
                  <HashtagLink>Trustless</HashtagLink>
                  <HashtagLink>Censorfree</HashtagLink>
                  <HashtagLink>Koos</HashtagLink>
                </p>
              </div>
            </div>
          </div>
        </div>

        <Footer />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts,
  loading: state.loading,
  connectedUser: state.connectedUser,
  registerError: state.registerError,
  registerSucceed: state.registerSucceed
});

const mapDispatchToProps = dispatch => ({
  onPageLoad: () => dispatch(PageLoad()),
  onRegister: data => dispatch(RegisterPageAttempt(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LandingPage);
