import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import { PostsPage } from "..";

import {
  Header,
  Footer,
  LayoutRightColumn,
  LayoutLeftColumn,
	NeedSignedIn,
  Post
} from "../../components";

import { PageLoad, SearchPageSearch } from "../../reducers";

/*
 *
 * This is the search page.
 *
 */
class SearchPage extends Component {
  constructor(props) {
    super();
    this.props = props;
  }

  componentDidMount() {
    const { method, target } = this.props.match.params;
    return this.props.onPageLoad().then(() => {
      console.log("[SearchPage - componentDidMount]");
      this.props.onSearchLoad({ method, target });
    });
  }

  render() {
    const { method, target } = this.props.match.params;
    return (
      <Fragment>
        <NeedSignedIn if={!this.props.connectedUser} to="/" />
        <Header {...this.props.connectedUser} search={target} />
        <div class="feed">
						
          <LayoutLeftColumn>
            <nav className="editprofilnav">
							{/*<a href={`#/search/profile/${target}`}>Profile</a> */}
							<p><a href={`#/search/posts/${target}`}>Posts</a></p>
							<p><a href={`#/search/hashtag/${target}`}>Hashtags</a></p>
            </nav>
          </LayoutLeftColumn>
          <LayoutRightColumn>
            {method === "posts" || method == "hashtag" ? (
              <PostsPage standAlone={true} {...this.props} />
            ) : (
              <p>HELLO</p>
            )}
          </LayoutRightColumn>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  searchResults: state.searchResults,
  connectedUser: state.connectedUser
});

const mapDispatchToProps = dispatch => ({
  onPageLoad: () => dispatch(PageLoad()),
  onSearchLoad: data => dispatch(SearchPageSearch(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchPage);
