import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import { NeedSignedIn, Footer, HashtagLink, Header } from "../../components";
import RegisterPage from "../register/register";
import { RegisterPageAttempt, PageLoad } from "../../reducers";

class AboutPage extends Component {
  constructor(props) {
    super();
    this.props = props;
  }

  componentDidMount() {
    this.props.onPageLoad();
    return {};
  }

  render() {
    return (
      <Fragment>
        <Header {...this.props.connectedUser} />
        <div className="project-text koos-intro-flex big" style={{textAlign: 'center'}}>
          <img
            src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/5769001/digitalchatnoir.png?width=64"
            alt="Chatnoir logo"
          />

          <p style={{textAlign: 'center'}}>
						<br />
						<br />
            Koos is a product of{" "}
            <a href="http://chatnoir.digital">Chatnoir Digital</a>. We are a
            worker-owned coop and a Decentralised Autonomous Organisation (DAO)
            dedicated to the finding socially empowering applications to
            open-source, decentralized, blockchain-based technologies. Don't
            hesitate to hit us up at our{" "}
            <a href="https://riot.im/app/#/room/!yKRGJWopsGxpcftwiU:matrix.org">
              riot.im
            </a>
          </p>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.loading,
  connectedUser: state.connectedUser
});

const mapDispatchToProps = dispatch => ({
  onPageLoad: () => dispatch(PageLoad())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AboutPage);
