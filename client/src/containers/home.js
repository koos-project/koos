import React, { Component } from "react";
import { connect } from "react-redux";
import { Post } from "../components/Post";
import { PostForm } from "../components/PostForm";
import { Header } from "../components/Header";
import { MainContent } from "../components/MainContent";
import { Footer } from "../components/Footer";

import { IndexPostSubmit, IndexLoadPosts } from "../reducers/actions";

class IndexPage extends Component {
  static getInitialProps({ store, isServer, pathname, query }) {
    store.dispatch(IndexLoadPosts());
    return {};
  }

  handleSubmit(event) {
    this.props.dispatch(IndexPostSubmit(event));
  }

  render() {
    return (
      <div>
        <Header />
        {this.props.posts.map((post, i) => (
          <div>
            <Post key={`post${i}`} post={post} />
            <hr />
          </div>
        ))}
        <PostForm onSubmit={event => this.handleSubmit(event)} />
        <MainContent />
        <Footer />
      </div>
    );
  }
}

export default connect(state => state)(IndexPage);
