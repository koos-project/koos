import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import {
  Footer,
  Header,
  LayoutLeftColumn,
  LayoutRightColumn,
  SettingsNavbar,
  NeedSignedIn,
  Profile,
  ProfileMine
} from "../../components";

import { ProfilePageUpdate, ProfilePageLoad, PageLoad } from "../../reducers";

class ProfilePage extends Component {
  constructor(props) {
    super();
    this.props = props;
  }

  componentDidMount() {
    this.props.onPageLoad().then(() => {
      const { userId } = this.props.match.params;
      if (userId) this.props.onProfileLoad(userId);
    });
  }

  render() {
    const { userId } = this.props.match.params;
    const connectedUserId = this.props.connectedUser
      ? this.props.connectedUser.userId
      : undefined;
    const onUpdate = update =>
      this.props.onProfileUpdate({
        ...this.props.connectedUser,
        ...update
      });

    return (
      <Fragment>
        <Header {...this.props.connectedUser} />
        <div className="profil-flex">
          <LayoutLeftColumn>
            <SettingsNavbar current="profile" />
          </LayoutLeftColumn>
          <LayoutRightColumn>
            {userId && userId !== connectedUserId ? (
              <Profile {...this.props.profileSelectedUser} />
            ) : (
              <Fragment>
                <NeedSignedIn if={!this.props.connectedUser} to="/" />
                <ProfileMine
                  onUpdate={onUpdate}
                  {...this.props.connectedUser}
                />
                {this.props.profileUpdateSucceed ? (
                  <p>
                    <strong>Your profile have correctly been updated</strong>
                  </p>
                ) : null}
              </Fragment>
            )}
          </LayoutRightColumn>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  connectedUser: state.connectedUser,
  profileSelectedUser: state.profileSelectedUser,
  profileUpdateSucceed: state.profileUpdateSucceed
});

const mapDispatchToProps = dispatch => ({
  onProfileUpdate: data => dispatch(ProfilePageUpdate(data)),
  onProfileLoad: id => dispatch(ProfilePageLoad(id)),
  onPageLoad: () => dispatch(PageLoad())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfilePage);
