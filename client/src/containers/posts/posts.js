/*
 *
 *   __  ___   ______     ______        _______.
 *  |  |/  /  /  __  \   /  __  \      /       |
 *  |  '  /  |  |  |  | |  |  |  |    |   (----`
 *  |    <   |  |  |  | |  |  |  |     \   \
 *  |  .  \  |  `--'  | |  `--'  | .----)   |
 *  |__|\__\  \______/   \______/  |_______/
 *
 **/
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import { hash } from "../../utils";

import {
  CommentForm,
  Comment,
  Footer,
  Header,
  LayoutLeftColumn,
  LayoutRightColumn,
  NeedSignedIn,
  Post,
  PostForm,
  ProfileIntroduction,
  SettingsNavbar
} from "../../components";

import {
  PostsPagePostSubmit,
  PostsPageCommentSubmit,
  PostsPageFollowUpdate,
  PostsPageLoadPosts,
  PageLoad
} from "../../reducers";

/*
 *
 * This is the post page.
 *
 * It will also be render in search results
 *
 */
class PostsPage extends Component {
  constructor(props) {
    super();
    this.props = props;
  }

  componentDidMount() {
    const userId = this.props.match.params.userId;
    this.props.onPageLoad().then(() => {
      if (!this.props.standAlone) this.props.onPostsLoad({ userId });
    });
  }

  render() {
    /*
     * This enrich the post with the connected user and a hash (postId)
     */
    const onSubmit = post => {
      this.props.onPostSubmit({
        ...post,
        ...this.props.connectedUser,
        postId: hash(post.content + post.timestamp)
      });
    };

    /*
     * This enrich the comment with the parent postId, the connectedUser
     * and a hash (the commentId)
     */
    const onCommentSubmit = comment => {
      this.props.onCommentSubmit({
        ...comment,
        ...this.props.connectedUser,
        postId: comment.parentPost.postId,
        commentId: hash(comment.content + comment.timestamp)
      });
    };

    /*
     * This define if the object is mine
     */
    const mine = (user, object) => user && user.userId === object.userId;

    /*
     * This comes from the route
     */
    const userId = this.props.match.params.userId;

    /*
     * This is triggered when the user want to follow someone
     */
    const onFollow = update =>
      this.props.onFollow({
        ...update,
        connectedUser: this.props.connectedUser
      });

    /*
     * This will render the comments for a post
     */
    const comments = post =>
      post.comments.map(comment => (
        <Fragment key={hash(comment)}>
          <Comment
            mine={mine(this.props.connectedUser, comment)}
            {...comment}
            onFollow={onFollow}
          />
          <hr />
        </Fragment>
      ));

    /*
     * Those are the actual posts
     */
    console.warn("POSTS PROPS CONNECTED USER", this.props.connectedUser);
    const posts = this.props.posts.map((post, i) => (
      <div className="feed-comment feed-flex-style" key={hash(post)}>
        <Post
          onFollow={onFollow}
          mine={mine(this.props.connectedUser, post)}
          {...post}
          connectedUser={this.props.connectedUser}
        />
        {post.comments && post.comments.length > 0 ? comments(post) : null}
        <CommentForm
          onSubmit={comment =>
            onCommentSubmit({
              ...comment,
              parentPost: post
            })
          }
          {...this.props.connectedUser}
        />
      </div>
    ));

    /*
     * This will be the only thing returned if standAlone is set (in the search page
     */
    console.log("[Post containers]", this.props.posts);
    const innerContent =
      this.props.posts.length === 0 ? (
        <p>
          <strong>No posts yet!</strong>
        </p>
      ) : (
        posts
      );

    /*
     * This will render the page itself
     *
     * It will only render the innerContent if standALone is set
     */
    return this.props.standAlone ? (
      innerContent
    ) : (
      <Fragment>
        <NeedSignedIn if={!this.props.connectedUser} to="/" />
        <Header {...this.props.connectedUser} />

        <div className="feed">
          <SettingsNavbar current="posts/me" />
          <LayoutLeftColumn>
            <ProfileIntroduction me {...this.props.connectedUser} />
          </LayoutLeftColumn>

          <LayoutRightColumn>
            <div className="personal-feed-publish personal-feed-main-comment feed-flex-style">
              {!userId || userId === "me" ? (
                <PostForm onSubmit={onSubmit} {...this.props.connectedUser} />
              ) : (
                <ProfileIntroduction {...this.props.posts[0]} />
              )}
            </div>
            {innerContent}
          </LayoutRightColumn>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  connectedUser: state.connectedUser,
  posts: state.posts,
  lading: state.loading
});

const mapDispatchToProps = dispatch => ({
  onPageLoad: () => dispatch(PageLoad()),
  onPostsLoad: () => dispatch(PostsPageLoadPosts()),
  onPostSubmit: post => dispatch(PostsPagePostSubmit(post)),
  onCommentSubmit: data => dispatch(PostsPageCommentSubmit(data)),
  onFollow: data => dispatch(PostsPageFollowUpdate(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostsPage);
