import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import { Header, SignInForm, Footer, NeedSignedIn } from "../../components";

import { SignInPageAttempt, PageLoad } from "../../reducers";

class SignInPage extends Component {
  constructor(props) {
    super();
    this.props = props;
  }

  componentDidMount() {
    this.props.onPageLoad();
    return {};
  }

  render() {
    return (
      <Fragment>
        <NeedSignedIn if={this.props.connectedUser} to="/posts" />
        <Header {...this.props.connectedUser} />
        <SignInForm onSignIn={this.props.onSignIn} />
        {this.props.signInError ? (
          <p>
            <strong class="error">{this.props.signInError.message}</strong>
          </p>
        ) : null}
        {this.props.signInSucceed ? (
          <p className="signinsuccess">
            <strong>
              You successfully signed in! You will be redirected soon.
            </strong>
          </p>
        ) : null}
        <Footer />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  signInSucceed: state.signInSucceed,
  signInError: state.signInError,
  connectedUser: state.connectedUser
});

const mapDispatchToProps = dispatch => ({
  onPageLoad: () => dispatch(PageLoad()),
  onSignIn: data => dispatch(SignInPageAttempt(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignInPage);
