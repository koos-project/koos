import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import { NeedSignedIn, Footer, HashtagLink, Header } from "../../components";
import RegisterPage from "../register/register";
import { RegisterPageAttempt, PageLoad } from "../../reducers";

class ProjectPage extends Component {
  constructor(props) {
    super();
    this.props = props;
  }

  componentDidMount() {
    this.props.onPageLoad();
    return {};
  }

  render() {
    return (
      <Fragment>
        <Header {...this.props.connectedUser} />

        <div className="koos-intro">
          <div className="koos-text-flex koos-flex">
            <div
              className="project-text koos-intro-flex big"
              style={{ textAlign: "center" }}
            >
              <p style={{ display: "inline" }}>
                <img
                  src="http://fluence.network/img/header/logo.svg"
                  alt="Fluence logo"
                />
                <img
                  style={{ width: "100px", heigth: "100px" }}
                  src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Ipfs-logo-1024-ice-text.png/220px-Ipfs-logo-1024-ice-text.png"
                  alt="IPFS logo"
                />
              </p>
              <br />
              <br />
              <br />
              <br />
              Koos have been built on the top of{" "}
              <a href="https://fluence.network">Fluence</a>, a decentralized
              cloud solution. It is hosted on{" "}
              <a href="https://ipfs.io/">
                IPFS, the Interplanetary File System
              </a>
              . As such, anybody can participate in the network and run a copy
              of Koos. It works in a purely peer-to-peer manner!
              <br />
              <br />
              It is entirely open-source and is protected by the GNU General
              Public Licence v3.
            </div>
          </div>
        </div>

        <Footer />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.loading,
  connectedUser: state.connectedUser
});

const mapDispatchToProps = dispatch => ({
  onPageLoad: () => dispatch(PageLoad())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectPage);
