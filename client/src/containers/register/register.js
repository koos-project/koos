import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import { Header, Footer, NeedSignedIn, RegisterForm } from "../../components";

import { RegisterPageAttempt, PageLoad } from "../../reducers";

class RegisterPage extends Component {
  constructor(props) {
    super();
    this.props = props;
  }

  componentDidMount() {
    this.props.onPageLoad();
    return {};
  }

  render() {
    return (
      <Fragment>
        <NeedSignedIn if={this.props.connectedUser} to="/posts" />

        {!this.props.standAlone ? (
          <Header {...this.props.connecteduser} />
        ) : null}

        <RegisterForm onSubmit={this.props.onRegister} />
        {this.props.registerError ? (
          <p>
            <strong>{this.props.registerError.message}</strong>
          </p>
        ) : null}
        {this.props.registerSucceed ? (
          <p>
            <strong>
              You successfully signed up! You will be redirected soon.
            </strong>
          </p>
        ) : null}
        {!this.props.standAlone ? <Footer /> : null}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  connectedUser: state.connectedUser,
  registerError: state.registerError,
  registerSucceed: state.registerSucceed
});

const mapDispatchToProps = dispatch => ({
  onPageLoad: () => dispatch(PageLoad()),
  onRegister: data => dispatch(RegisterPageAttempt(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterPage);
