import sha1 from "js-sha1";

export const hash = object => sha1(JSON.stringify(object));
export const findHashtags = str =>
  str.split(" ").filter(v => v.startsWith("#"));
export const stripFirstChar = str => str.substr(1, str.length);
export const stripHashtags = str =>
  str.split(" ").filter(v => !v.startsWith("#"));
export const v = setFn => e => setFn(e.target.value);
export const from = value => new Promise(resolve => resolve(value));
export const timeout = (delay, answer) =>
  new Promise(r => {
    setTimeout(() => r(answer), delay);
  });
export const stop = e => e.preventDefault();
export const onEnter = fn => e => {
  if (e.keyCode === 13 && e.shiftKey === false) {
    stop(e);
    fn(e);
  }
};
export const now = () => Math.floor(new Date().getTime() / 1000);
