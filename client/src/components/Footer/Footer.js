import React from "react";
import { version } from "../../../package.json";

export const Footer = props => (
  <div className="footer">
    <div className="footer-flex">
      <img src="logo.png" alt="Footer logo" />
      <p>
        2019 GNU GENERAL PUBLIC LICENCE v3. Product of{" "}
        <a href="http://chatnoir.digital">Chatnoir Digital.</a>. Powered by{" "}
        <a href="http://fluence.network">Fluence</a>. Koos v.{version}.
      </p>
    </div>
  </div>
);
