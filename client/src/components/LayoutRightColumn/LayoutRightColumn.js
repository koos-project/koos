import React from "react";

export const LayoutRightColumn = props => (
  <div className="feed-right-content profil-flex-content">{props.children}</div>
);
