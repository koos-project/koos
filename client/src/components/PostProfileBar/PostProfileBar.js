import React, { Fragment } from "react";

import { ProfileButton } from "..";

import { Icon } from "@material-ui/core";

export const PostProfileBar = props => (
  <div className="profil-interaction">
    <ProfileButton userId={props.userId} />
    {props.followed ? (
      <button
        onClick={e => props.onFollow(false)}
        className="btn btn-follow btn-unfollow"
        alt="#"
      >
        Unfollow <Icon>clear</Icon>
      </button>
    ) : (
      <button
        onClick={e => props.onFollow(true)}
        className="btn btn-follow"
        alt="#"
      >
        Follow <Icon>done</Icon>
      </button>
    )}
  </div>
);
