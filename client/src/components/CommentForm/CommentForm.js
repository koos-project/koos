import React, { useState } from "react";

import { v, onEnter, now } from "../../utils";

export const CommentForm = props => {
  const [content, setContent] = useState("");
  const onKeyDown = onEnter(() => {
    console.log("onKeyDown");
    props.onSubmit({ content, timestamp: now() });
  });

  return (
    <div className="personal-feed-publish-info">
      <div className="personal-feed-publish-pp">
        <img
          className="circular-crop-medium"
          src={props.avatar}
          alt="mainprofile"
        />
      </div>
      <div className="personal-feed-publish-text">
        <h2> {props.name} </h2>
        <textarea
          value={content}
          onChange={v(setContent)}
          onKeyDown={e => {
            console.log("keydown");
            onKeyDown(e);
          }}
          placeholder="Write your comment here"
        />
      </div>
    </div>
  );
};
