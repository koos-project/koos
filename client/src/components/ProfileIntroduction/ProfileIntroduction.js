import React, { Fragment } from "react";

import { withRouter } from "react-router-dom";

const EditProfileButton = withRouter(({ history }) => (
  <button
    onClick={() => history.push("/profile")}
    className="btn btn-follow btn-editprofil"
  >
    Edit profil
  </button>
));

const MyPostsButton = withRouter(({ history }) => (
  <button
    onClick={() => history.push("/posts/me")}
    className="btn btn-follow btn-editprofil"
  >
    My posts
  </button>
));

export const ProfileIntroduction = props => (
  <Fragment>
    <img className="circular-crop-large" src={props.avatar} alt="mainprofile" />
    <h2>{props.name}</h2>
    <p className="profildescritpion"> {props.presentation}</p>

    <div className="personal-feed-content-info">
      {/* PROFILE INFO*/}
      {props.me ? (
        <Fragment>
          <EditProfileButton />
          <MyPostsButton />
        </Fragment>
      ) : null}
    </div>
  </Fragment>
);

// <p className="content-info">
//   {" "}
//   {props.hashtag} <strong classNames="strong">#</strong>hastags{" "}
// </p>
// <p className="content-info">
//   {" "}
//   <strong classNames="strong">{props.followers}</strong> Followers
// </p>
