import React from "react";

export const HashtagLink = ({ children }) => (
  <span className="hashtag-info" href={`/search/hashtag/${children}`}>
    <strong className="strong">&#35;</strong> {children}
  </span>
);
