import React, { Fragment, useState } from "react";

import { HashtagLink } from "../";

import * as utils from "../../utils";

export const PostForm = props => {
  const [content, setContent] = useState("");
  const [hashtags, setHashtags] = useState([]);

  console.info("PostForm: props", props);

  const onSubmit = () => {
    const hashtags = utils.findHashtags(content);
    console.info("[PostForm - onSubmit]: hashtags");
    const postWithoutId = {
      content,
      hashtags,
      timestamp: utils.now()
    };
    const post = {
      ...postWithoutId,
      postId: utils.hash(postWithoutId.content)
    };
    props.onSubmit(post);
    setContent("");
    setHashtags([]);
  };

  const onContentChange = content => {
    setHashtags(utils.findHashtags(content).map(utils.stripFirstChar));
    setContent(content);
  };

  const onKeyDown = utils.onEnter(() => onSubmit());

  return (
    <Fragment>
      <h2>
        <i class="material-icons personicon">create</i> Write a post
      </h2>
      <div className="personal-feed-publish-info">
        <div className="personal-feed-publish-pp">
          <img
            className="circular-crop-medium"
            src={props.avatar}
            alt="mainprofile"
          />
        </div>
        <div className="personal-feed-publish-text">
          <textarea
            value={content}
            onChange={utils.v(onContentChange)}
            onKeyDown={onKeyDown}
            placeholder="Write your comment here"
          ></textarea>
          {hashtags.map(hashtag => (
            <HashtagLink key={utils.hash(hashtag)}>{hashtag}</HashtagLink>
          ))}
        </div>
      </div>
      <button className="btn btn-1 icon-arrow-right" onClick={onSubmit}>
        Publish<i className="material-icons">arrow_forward</i>
      </button>
    </Fragment>
  );
};
