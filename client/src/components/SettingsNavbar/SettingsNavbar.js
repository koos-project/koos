import React from "react";

const addHref = (props, that) =>
  props.current === that ? null : { href: `#/${that}` };

export const SettingsNavbar = props => (
  <nav className="editprofilnav">
    <p>
      <a {...addHref(props, "profile")}>Profile</a>
    </p>
    <p>
      <a {...addHref(props, "sign-out")}>Sign out</a>
    </p>
  </nav>
);

// <p>
//   <a {...addHref(props, "posts/me")}>My posts</a>
// </p>
// <p>
//   <a {...addHref(props, "settings")}>Settings</a>
// </p>
