import React from "react";

export const LayoutLeftColumn = props => (
  <div className="personal-feed-content feed-flex-style feed-left-content profil-flex-content">
    {props.children}
  </div>
);
