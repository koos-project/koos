import React from "react";

import { PostProfileBar } from "../";

export const Comment = props => (
  <div className="personal-feed-publish-info">
    <div className="personal-feed-publish-pp">
      <img
        className="circular-crop-medium"
        src={props.avatar}
        alt="mainprofile"
      />
      {!props.mine ? (
        <PostProfileBar
          onFollow={following =>
            props.onFollow({ userId: props.userId, following })
          }
          following={props.following.split(",").includes(props.userId)}
          userId={props.userId}
        />
      ) : null}
    </div>
    <div className="personal-feed-publish-text">
      <h2> {props.name}</h2>
      <p>{props.content}</p>
    </div>
  </div>
);
