import React, { Fragment, useState } from "react";

import { v } from "../../utils";

export const RegisterForm = props => {
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");
  const [email, setEmail] = useState("");
  const [presentation, setPresentation] = useState("");

  const [validationError, setValidationError] = useState("");
  const checkError = () => {
    if (name.trim() === "") return "Username cannot be blank";
    if (email.trim() === "") return "Email cannot be blank";
    if (presentation.trim() === "") return "Presentation cannot be blank";
    if (password.trim() === "") return "Password cannot be blank";
    if (passwordConfirm !== password)
      return "Password confirmation doesn't match";
  };

  const onSubmit = event => {
    event.preventDefault();
    const error = checkError();
    if (error) setValidationError(error);
    props.onSubmit({ name, email, presentation, password });
  };

  return (
    <div className="bodygeneral">
      <div className="contenucenter">
        <h1> Create an account </h1>
        <form className="registerform">
          <div className="input-register">
            <input
              type="text"
              value={name}
              onChange={v(setName)}
              name="name"
              placeholder="Name"
            />
          </div>
          <div className="input-register">
            <input
              type="email"
              value={email}
              onChange={v(setEmail)}
              name="email"
              placeholder="Email"
            />
          </div>
          <div className="input-register">
            <input
              type="password"
              value={password}
              onChange={v(setPassword)}
              name="password"
              placeholder="Password"
            />
          </div>
          <div className="input-register">
            <input
              type="password"
              value={passwordConfirm}
              name="password_confirm"
              onChange={v(setPasswordConfirm)}
              placeholder="Confirm your password"
            />
          </div>
          <div className="input-register">
            <textarea
              value={presentation}
              onChange={v(setPresentation)}
              name="presentation"
              placeholder="Presentation"
            ></textarea>
          </div>
          {validationError ? <strong>{validationError}</strong> : null}
          <button
            onClick={onSubmit}
            className="btn btn-follow btn-signin"
            type="submit"
          >
            Let's go!
          </button>
        </form>
      </div>
    </div>
  );
};
