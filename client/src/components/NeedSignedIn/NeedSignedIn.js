import React from "react";

import { Redirect } from "react-router";

export const NeedSignedIn = props =>
  props.if ? <Redirect to={props.to} /> : null;
