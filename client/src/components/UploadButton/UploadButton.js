import React, { useState, Fragment } from "react";

export const UploadButton = props => {
  const [inputRef, setInputRef] = useState("inputRef");
  const onUpload = event => {
    const file = event.target.files[0];
    if (!file.type.match("image.*")) return;
    const reader = new FileReader();
    reader.onload = event => props.onSubmit(event.target.result);
    reader.readAsDataURL(file);
    event.stopPropagation();
  };

  return (
    <Fragment>
      <input
        type="file"
        style={{ display: "none" }}
        onChange={onUpload}
        ref={setInputRef}
      />
      <button onClick={() => inputRef.click()}>Change avatar</button>
    </Fragment>
  );
};
