import React from "react";

import { withRouter } from "react-router-dom";

const MyPostsButton = withRouter(({ history, userId }) => (
  <button
    onClick={() => history.push(`/posts/${userId}`)}
    className="btn btn-1 btn-editprofil"
  >
    My posts
  </button>
));

const FriendListItem = withRouter(({ history, user }) => (
  <div>
    <img
      src={user.avatar}
      className="circular-crop-small"
      alt={`${user.name} avatar`}
    />
    <p>{user.name}</p>
    <button
      className="btn btn-follow"
      onClick={() => history.push(`/profile/${user.userId}`)}
    >
      Profile
    </button>
  </div>
));

export const Profile = props => (
  <div className=" feed-comment feed-flex-style editprofile">
    <h2>{props.name}</h2>
    <div className="pictureProfile">
      <img
        className="circular-crop-medium"
        src={process.env.PUBLIC_URL + props.avatar}
        alt="mainprofile"
      />
    </div>
    <h3>Presentation:</h3>
    <p className="profildescritpion">{props.presentation}</p>
    <h3>Following: </h3>
    <div className="followingprofile">
      {props.following
        ? props.following.map(user => <FriendListItem user={user} />)
        : null}
    </div>
    <MyPostsButton userId={props.userId} />
  </div>
);
