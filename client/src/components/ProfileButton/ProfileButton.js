import React from "react";

import { withRouter } from "react-router-dom";
import { Icon } from "@material-ui/core";

export const ProfileButton = withRouter(({ history, userId }) => (
  <button
    onClick={() => history.push(`/profile/${userId}`)}
    className="btn btn-follow btn-unfollow"
    alt="#"
  >
    Profile <Icon>cross-red</Icon>
  </button>
));
