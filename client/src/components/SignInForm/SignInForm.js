import React, { Fragment, useState } from "react";

import { v } from "../../utils";

export const SignInForm = props => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const onSignIn = e => {
    e.preventDefault();
    props.onSignIn({ email, password });
  };

  return (
    <div className="bodygeneral">
      <div className="contenucenter">
        <h1>Welcome back!</h1>
        <form className="registerform signinform">
          <div className="input-register">
            <input
              type="text"
              name="email"
              value={email}
              onChange={v(setEmail)}
              placeholder="Email"
            />
          </div>
          <div className="input-register">
            <input
              type="password"
              name="password"
              value={password}
              onChange={v(setPassword)}
              placeholder="Password"
            />
          </div>
          <button
            onClick={onSignIn}
            className="btn btn-follow btn-signin"
            type="submit"
          >
            Let's go!
          </button>
        </form>
      </div>
    </div>
  );
};
