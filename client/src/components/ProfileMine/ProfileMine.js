import React, { Fragment, useState } from "react";

import { v, onEnter } from "../../utils";

export const ProfileMine = props => {
  const [presentation, setPresentation] = useState(props.presentation);
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");
  const [passwordError, setPasswordError] = useState();
  const [avatarPanelOpen, setAvatarPanelOpen] = useState(false);
  const [avatarURL, setAvatarURL] = useState(false);

  const onKeyDownAvatarURL = onEnter(() =>
    props.onUpdate({ update: "avatar", avatarURL })
  );

  const onKeyDownPresentation = onEnter(() =>
    props.onUpdate({ update: "presentation", presentation })
  );
  const onKeyDownPassword = onEnter(() =>
    password === passwordConfirm
      ? props.onUpdate({ update: "password", password })
      : setPasswordError("Password confirmation doesn't match.")
  );
  const onAvatarUpdate = avatar => props.onUpdate({ update: "avatar", avatar });

  return (
    <div className="feed-edit-profile feed-comment feed-flex-style">
      <h2>{props.name}</h2>
      <div className="pictureProfile">
        <img
          className="circular-crop-medium"
          src={props.avatar}
          alt="mainprofile"
        />
      </div>
      <button
        className="btn btn-editprofil btn-follow btn-editavatar"
        onClick={() => setAvatarPanelOpen(true)}
      >
        Change avatar
      </button>

      {avatarPanelOpen ? (
        <Fragment>
          <h3>Choose a predefined picture image</h3>
          <div className="pictureProfile">
            <img
              src="pp1.png"
              alt="Cute cat"
              className="circular-crop-medium"
              onClick={() => onAvatarUpdate("pp1.png")}
            />
          </div>
          <div className="pictureProfile">
            <img
              src="pp2.png"
              alt="Even cuter cat"
              className="circular-crop-medium"
              onClick={() => onAvatarUpdate("pp2.png")}
            />
          </div>
          <div className="pictureProfile">
            <img
              src="pp3.png"
              alt="Awfully cute cat"
              className="circular-crop-medium"
              onClick={() => onAvatarUpdate("pp3.png")}
            />
          </div>
          <div className="pictureProfile">
            <img
              src="pp4.png"
              alt="The website creator cat"
              className="circular-crop-medium"
              onClick={() => onAvatarUpdate("pp4.png")}
            />
          </div>
          <p>Or enter a valid URL here</p>
          <input
            type="text"
            value={avatarURL}
            onChange={v(setAvatarURL)}
            onKeyDown={onKeyDownAvatarURL}
          />
        </Fragment>
      ) : null}
      <textarea
        value={presentation}
        onChange={v(setPresentation)}
        onKeyDown={onKeyDownPresentation}
      ></textarea>
      <input
        type="password"
        value={password}
        onChange={v(setPassword)}
        placeholder="Password"
        onKeyDown={onKeyDownPassword}
      />
      <input
        type="password"
        value={passwordConfirm}
        onChange={v(setPasswordConfirm)}
        placeholder="Confirm password"
        onKeyDown={onKeyDownPassword}
      />
      {passwordError ? <strong>{passwordError}</strong> : null}
    </div>
  );
};
