import React, { Fragment } from "react";

import { HashtagLink, PostProfileBar } from "..";

/*
 * TODO: reecrire le mine avec un HOC
 */

export const Post = props => (
  <Fragment>
    <h2>
      {" "}
      <i class="material-icons personicon">person</i> {props.name}{" "}
    </h2>
    <div className="personal-feed-publish-info">
      <div className="personal-feed-publish-pp">
        <img
          className="circular-crop-medium"
          src={process.env.PUBLIC_URL + props.avatar}
          alt="mainprofile"
        />
      </div>
      {!props.mine ? (
        <PostProfileBar
          onFollow={following =>
            props.onFollow({ userId: props.userId, following })
          }
          followed={props.connectedUser.following
            .split(",")
            .includes(props.userId)}
          userId={props.userId}
        />
      ) : null}
      <div className="personal-feed-publish-text">
        <p>{props.content}</p>
        {props.hashtags.map(hashtag => (
          <HashtagLink>{hashtag}</HashtagLink>
        ))}
      </div>
    </div>
  </Fragment>
);
