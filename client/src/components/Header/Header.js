import React, { Fragment, useState } from "react";

import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";

import { v, onEnter } from "../../utils";

const MyPostIcon = withRouter(({ history, avatar }) => (
  <div
    className="pictureProfile flexHeader"
    onClick={() => history.push("/posts/me")}
  >
    <img className="circular-crop-small" src={avatar} alt="mainprofile" />
  </div>
));

export const Header = withRouter(props => {
  const [search, setSearch] = useState(props.search ? props.search : "");
  const searchOnEnter = onEnter(() => {
    props.history.push(`/search/posts/${search}`);
  });

  return (
    <div className="header">
      <div className="headerMain">
        <Link className="mainLogo flexHeader" to="/">
          <img src={process.env.PUBLIC_URL + "../logo.png"} alt="KOOS logo" />
        </Link>
        {props.userId ? (
          <Fragment>
            <form className="searchBtn flexHeader" action="#">
              <input
                type="text"
                name="search"
                value={search}
                onChange={v(setSearch)}
                onKeyDown={searchOnEnter}
								placeholder="Search Koos"
              />
            </form>
            <MyPostIcon avatar={props.avatar} />
          </Fragment>
        ) : (
          <div className="menu-bar">
            <a href="#/">Home</a>
            <a href="#/project">Project</a>
            <a href="#/sign-in">Sign in</a>
            <a href="#/register">Register</a>
            <a href="#/about">About</a>
          </div>
        )}
      </div>
    </div>
  );
});
