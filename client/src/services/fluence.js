/*
 *
 *   __  ___   ______     ______        _______.
 *  |  |/  /  /  __  \   /  __  \      /       |
 *  |  '  /  |  |  |  | |  |  |  |    |   (----`
 *  |    <   |  |  |  | |  |  |  |     \   \
 *  |  .  \  |  `--'  | |  `--'  | .----)   |
 *  |__|\__\  \______/   \______/  |_______/
 *
 **/
import * as fluence from "fluence";

import config from "../config/config.json";
import { from } from "../utils";

/*
 * This encode the JSON in plain-text
 */
const plaintextEncode = action => {
  console.info("plaintextEncode():", action);
  if (action.action === "signIn")
    return `signIn
${action.email}
${action.password}
`;
  else if (action.action === "addUser")
    return `addUser
${action.userId}
${action.name}
${action.email}
${action.password}
${action.presentation}
`;
  else if (action.action === "getMe")
    return `getMe
${action.token}
`;
  else if (action.action === "getPosts")
    return `getPosts
${action.token}
${action.userId}
`;
  else if (action.action === "addPost")
    return `addPost
${action.token}
${action.postId}
${action.userId}
${action.content}
${action.timestamp}
${action.hashtags}
`;
  else if (action.action === "getUser")
    return `getUser
${action.token}
${action.userId}`;
  else if (action.action === "updateUser" && action.update === "presentation")
    return `updateUser
${action.userId}
${action.token}
presentation
${action.presentation}`;
  else if (action.action === "updateUser" && action.update === "password")
    return `updateUser
${action.userId}
${action.token}
presentation
${action.password}`;
  else if (action.action === "updateUser" && action.update === "avatar")
    return `updateUser
${action.userId}
${action.token}
avatar
${action.avatar}`;
  else if (action.action === "addComment")
    return `addComment
${action.commentId}
${action.token}
${action.userId}
${action.postId}
${action.content}
${action.timestamp}`;
  else if (action.action === "getComments")
    return `getComments
${action.postId}
${action.token}`;
  else if (action.action === "search")
    return `search
${action.token}
${action.method}
${action.target}`;
  else if (action.action === "updateFollow")
    return `updateFollow
${action.token}
${action.connectedUser.userId}
${action.userId}
${action.following}`;
};

export class FluenceGateway {
  constructor(session) {
    this.session = session;
    this.fluence = undefined;
  }
  init() {
    console.log("Attempting to connect", config);

    if (config.CONNECTION_METHOD === "local") {
      let appId = 1;
      this.fluence = fluence.directConnect("localhost", 30000, appId);

      return from(true);
    } else {
      return fluence
        .connect(
          config.FLUENCE_CONTRACT_ADDRESS,
          config.FLUENCE_APP_ID,
          config.FLUENCE_ETHEREUM_URL
        )
        .then(fluence => {
          this.fluence = fluence;
          console.log("fluence fluence opened", this.fluence);
        });
    }
  }

  call(actionTokenLess) {
    const action = { ...actionTokenLess, token: this.session.token };
    console.warn("Calling fluence with ", plaintextEncode(action));

    if (!this.fluence) throw Error("Fluence haven't been initialised");

    const request = this.fluence.request(
      plaintextEncode(action)
        .split("\n")
        .join("#$#")
    );

    return request
      .result()
      .then(answer => answer.asString())
      .then(answer => JSON.parse(answer))
      .then(answer => {
        console.warn("Answer from fluence", answer);
        if (answer.answer >= 400) throw answer;
        return answer;
      });

    // if (action.action === 'getPosts') {
    //   console.info('GetPostsReturning', postsMock);
    //   return from(postsMock);
    // }
    // if (action.action == 'addPost') return from({code: 201});
    // if (action.action == 'addComment') return from({code: 201});
    // if (action.action == 'getMe') {
    //   if (!action.token) throw Error('getMe should have a token');
    //   return from({code: 200, ...userMock()});
    // }
    // if (action.action == 'signIn')
    //   return from({code: 200, token: hash(new Date())});
    // if (action.action == 'addUser') {
    //   if (
    //     !action.name ||
    //     !action.email ||
    //     !action.password ||
    //     !action.presentation ||
    //     !action.userId
    //   )
    //     throw Error('addUser missing variables' + JSON.stringify(action));
    //   return from({code: 201});
    // }
    // if (action.action == 'updateProfile') return from({code: 200});
    // if (action.action == 'getProfile')
    //   return from({
    //     code: 200,
    //     user: usersMock().filter(user => user.userId == action.userId)[0],
    //   });
    // if (action.action == 'getComment')
    //   return from({
    //     code: 200,
    //     comment: commentsMock().filter(
    //       comment => comment.commentId == action.commentId,
    //     )[0],
    //   });
  }
}
