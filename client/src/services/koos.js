/*
 *
 *   __  ___   ______     ______        _______.
 *  |  |/  /  /  __  \   /  __  \      /       |
 *  |  '  /  |  |  |  | |  |  |  |    |   (----`
 *  |    <   |  |  |  | |  |  |  |     \   \
 *  |  .  \  |  `--'  | |  `--'  | .----)   |
 *  |__|\__\  \______/   \______/  |_______/
 *
 **/

import { FluenceGateway } from "./fluence";
import { SessionManager } from "./session";

import { hash, from } from "../utils";

/*
 * TODO: parse hashtags in the server
 * This is ugly as fuck
 * See issue #45
 */
const emptyStringArrayToArray = array =>
  (array.length === 1) & (array[0] === "") ? [] : array;
const parseHashtags = post => ({
  ...post,
  hashtags: emptyStringArrayToArray(post.hashtags.split(","))
});

class koosService {
  constructor() {
    this.session = new SessionManager();
    this.fluence = new FluenceGateway(this.session);
  }

  init() {
    return this.fluence.init().then(() =>
      /* TODO See issue #29
       * and reducers/actions.js: 110
       */
      this.session.token
        ? this.fluence.call({ action: "getMe" }).then(answer => {
            console.info("koos.init: Recieved an answer", answer);
            return answer;
          })
        : undefined
    );
  }

  enrichWithUserData(object) {
    console.warn("[enrichWithUserData]", object);
    return this.fluence
      .call({ action: "getUser", userId: object.userId })
      .then(answer => {
        console.info("loadPosts - enrich with user data");
        console.info("object", object);
        console.info("answer", answer);
        // Enrich the object with user data
        return { ...object, ...answer.user, following: answer.user.following };
      });
  }

  enrichWithComments(post) {
    const { postId } = post;
    console.log("enrichWithComments");

    return this.fluence
      .call({ action: "getComments", postId })
      .then(({ comments }) =>
        Promise.all(comments.map(comment => this.enrichWithUserData(comment)))
      )
      .then(comments => ({ ...post, comments }));
  }

  /*
   * TODO: Create a cache to store user datas, this will leads to quite
   * a lot of redundant queries
   */
  loadPosts(userId) {
    return this.fluence.call({ action: "getPosts", userId }).then(answer => {
      console.info("loadPosts: Recieved an answer", answer);

      return Promise.all(
        answer.posts.map(post =>
          this.enrichWithComments(parseHashtags(post)).then(post =>
            this.enrichWithUserData(post)
          )
        )
      );
    });
  }

  addPost(post) {
    console.log("[Koos service - addPost]:", post);
    return this.fluence
      .call({
        action: "addPost",
        ...post,
        hashtags: post.hashtags.join(",")
      })
      .then(answer => {
        console.info("postPost: Recieved an answer", answer);
        return { post, answer };
      });
  }

  addComment(comment) {
    return this.fluence
      .call({
        action: "addComment",
        ...comment,
        postId: comment.parentPost.postId,
        commentId: hash(comment.content + comment.parentPost.postId)
      })
      .then(answer => {
        console.info("AddComment: Recieved an answer", answer);
        return { comment, answer };
      });
  }

  signIn(data) {
    const { email, password } = data;
    return this.fluence
      .call({
        action: "signIn",
        email,
        password: hash(password)
      })
      .then(answer => {
        console.info("signin: Recieved an answer", answer);
        this.session.token = answer.token;
        return this.fluence
          .call({
            action: "getMe"
          })
          .then(getMeAnswer => ({ ...answer, user: getMeAnswer.user }));
      });
  }

  register(data) {
    return this.fluence
      .call({
        action: "addUser",
        ...data,
        password: hash(data.password)
      })
      .then(answer => {
        console.info("register: Recieved an answer", answer);
        return { answer };
      });
  }

  signOut() {
    this.session.token = null;
    return from(true);
  }

  updateProfile(data) {
    return this.fluence
      .call({
        action: "updateUser",
        ...data
      })
      .then(answer => {
        console.info("updateProfile: recieved an answer", answer);
        return { answer };
      });
  }

  getProfile(userId) {
    return this.fluence
      .call({
        action: "getUser",
        userId: userId
      })
      .then(({ user }) => {
        console.log("trying to parse user.following", user.following);
        return user.following
          ? Promise.all(
              /*
               * TODO: Fix this, this is ugly
               */
              user.following.split(",").map(userId => {
                return this.fluence
                  .call({ action: "getUser", userId })
                  .then(({ user }) => user);
              })
            ).then(followedUsers => ({ ...user, following: followedUsers }))
          : Promise.resolve([]);
      })
      .then(user => {
        console.info("getProfile: recieved an answer", user);
        return { user };
      });
  }

  search(data) {
    return this.fluence
      .call({
        action: "search",
        ...data
      })
      .then(answer => {
        console.info("getProfile: recieved an answer", answer);
        const { users, posts } = answer;
        if (data.method === "profile") return Promise.resolve({ users });
        // Enrich posts with users datas and comments
        else
          return Promise.all(
            answer.posts.map(post =>
              this.enrichWithComments(parseHashtags(post)).then(post =>
                this.enrichWithUserData(post)
              )
            )
          );
      });
  }

  followUpdate(update) {
    return this.fluence
      .call({
        action: "updateFollow",
        ...update
      })
      .then(answer => {
        console.info("followUpdate: recieved an answer", answer);
        return update;
      });
  }
}

export const koos = new koosService();
