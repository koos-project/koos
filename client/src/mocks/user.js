import { hash } from "../utils";

export const userMock = n => ({
  userId: hash("bertha"),
  avatar: "user1.png",
  name: "Bertha Kanta",
  friends: [hash(123), hash(456), hash(789)],
  email: "bertha.kanta@gmail.com",
  presentation: "Bitch I am the best!"
});

export const usersMock = () => [
  {
    userId: hash("charles"),
    name: "Charles Grandma",
    avatar: "mainpp.png",
    email: "charlegrandma@caramail.com",
    presentation:
      "I am Charles Grandma, yes, that is my name, and trust me, I heard all the jokes",
    following: [hash("marc"), hash("bertha")]
  },
  {
    userId: hash("bertha"),
    name: "Bertha Kanta",
    avatar: "user1.png",
    email: "berthakanta@gmail.com",
    presentation: "I like supertramp. I do not know why.",
    following: [hash("charle"), hash("marc")]
  },
  {
    userId: hash("marc"),
    name: "Marc o'Connoll",
    avatar: "user2.png",
    email: "marcoconnoll@gmail.com",
    presentation: "In this day and age, you do not know what is real anymore.",
    following: [hash("bertha")]
  },
  {
    userId: hash("quentin"),
    name: "Quentin Tarentina",
    avatar: "user2.png",
    email: "quentin@gmail.com",
    presentation: "I made a new movie recently.",
    following: [hash("marc")]
  }
];
