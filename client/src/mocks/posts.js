import { hash } from "../utils";

export const postsMock = {
  code: 200,
  posts: [
    {
      postId: hash(123),
      author: hash("bertha"),
      hashtags: ["koos", "permaweb", "decentralisation", "blockchain"],
      content:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam malesuada sodales tortor, rhoncus lobortis mauris. Curabitur finibus sit amet elit eu tincidunt. Morbi vestibulum vitae mauris blandit pretium. Fusce pretium vulputate turpis, sed ultricies enim condimentum eu. Duis quis laoreet ex. Ut eros quam, sodales at elit sed, cursus pellentesque leo. Cras viverra imperdiet lectus, sit amet pretium ligula tincidunt non. Aenean elit nisi, elementum at fermentum a, facilisis quis velit. In sodales lacus at sodales volutpat. Fusce eu sodales nisi, vel rutrum ante. Sed felis ligula, aliquet blandit faucibus at, cursus vel ipsum. Donec facilisis libero nulla, a consectetur lacus bibendum in. Donec volutpat magna eleifend lectus hendrerit pharetra.",
      comments: [hash(456), hash(789)]
    },
    {
      postId: hash(123),
      author: hash("charles"),
      hashtags: ["koos", "grand", "garcon", "brel"],
      content:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam malesuada sodales tortor, rhoncus lobortis mauris. Curabitur finibus sit amet elit eu tincidunt. Morbi vestibulum vitae mauris blandit pretium. Fusce pretium vulputate turpis, sed ultricies enim condimentum eu. Duis quis laoreet ex. Ut eros quam, sodales at elit sed, cursus pellentesque leo. Cras viverra imperdiet lectus, sit amet pretium ligula tincidunt non. Aenean elit nisi, elementum at fermentum a, facilisis quis velit. In sodales lacus at sodales volutpat. Fusce eu sodales nisi, vel rutrum ante. Sed felis ligula, aliquet blandit faucibus at, cursus vel ipsum. Donec facilisis libero nulla, a consectetur lacus bibendum in. Donec volutpat magna eleifend lectus hendrerit pharetra.",
      comments: []
    },
    {
      postId: hash(12123),
      hashtags: ["food", "be", "good"],
      author: hash("quentin"),
      content:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam malesuada sodales tortor, rhoncus lobortis mauris. Curabitur finibus sit amet elit eu tincidunt. Morbi vestibulum vitae mauris blandit pretium. Fusce pretium vulputate turpis, sed ultricies enim condimentum eu. Duis quis laoreet ex. Ut eros quam, sodales at elit sed, cursus pellentesque leo. Cras viverra imperdiet lectus, sit amet pretium ligula tincidunt non. Aenean elit nisi, elementum at fermentum a, facilisis quis velit. In sodales lacus at sodales volutpat. Fusce eu sodales nisi, vel rutrum ante. Sed felis ligula, aliquet blandit faucibus at, cursus vel ipsum. Donec facilisis libero nulla, a consectetur lacus bibendum in. Donec volutpat magna eleifend lectus hendrerit pharetra.",
      comments: []
    }
  ]
};

export const newPostMock = (content, hashtags) => ({
  postId: hash(Math.random()),
  hashtags,
  content,
  comments: []
});
