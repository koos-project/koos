import { hash } from "../utils";

/*
 * TODO: rewrite every id with the type (id => userId)
 * Be careful about the services
 */

export const newCommentMock = (content, author) => ({
  commentId: hash({ content, author }),
  author,
  content
});

export const commentsMock = () => [
  {
    commentId: hash(456),
    author: hash("marc"),
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam malesuada sodales tortor, rhoncus lobortis mauris. Curabitur finibus sit amet elit eu tincidunt."
  },
  {
    commentId: hash(789),
    author: hash("bertha"),
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam malesuada sodales tortor, rhoncus lobortis mauris. Curabitur finibus sit amet elit eu tincidunt."
  },
  {
    commentId: hash(101112),
    author: hash("bertha"),
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam malesuada sodales tortor, rhoncus lobortis mauris. Curabitur finibus sit amet elit eu tincidunt."
  }
];
