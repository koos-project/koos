export const initialState = {
  posts: [],
  loading: true,
  connectedUser: null,
  signInSucceed: false,
  signInError: null,
  registerSucceed: false,
  registerError: null,
  profileUpdateError: null,
  profileUpdateSucceed: false,
  profileSelectedUser: null,
  profileLoadError: null
};

const logger = reducer => {
  return (state, action) => {
    console.log("Action", action);
    console.log("Previous state", state);

    const newState = reducer(state, action);

    console.log("New state", newState);

    return newState;
  };
};

export const reducer = logger((state = initialState, action) => {
  switch (action.type) {
    case "index/load/success":
      return { ...state, loading: false };

    case "posts/posts/load/success":
      return { ...state, posts: action.payload };
    case "posts/post/success": {
      const userEnrichedPost = {
        ...state.connectedUser,
        ...action.payload
      };
      const posts = [].concat([userEnrichedPost], state.posts);
      return { ...state, posts };
    }
    case "posts/comment/success": {
      const comment = action.payload;
      const { parentPost } = action.payload;
      const userEnrichedComment = {
        ...comment,
        ...state.connectedUser
      };
      console.info(
        "Reducer - posts/comment/success: userEnrichedComment",
        userEnrichedComment
      );
      const updatedPost = {
        ...parentPost,
        comments: parentPost.comments
          ? [].concat(parentPost.comments, [userEnrichedComment])
          : [userEnrichedComment]
      };
      const posts = state.posts.map(post =>
        post.postId === parentPost.postId ? updatedPost : post
      );
      return { ...state, posts };
    }

    /* TODO use toast */
    case "sign-in/attempt/success/signal": {
      return { ...state, signInSucceed: true, signInError: null };
    }
    case "sign-in/attempt/success": {
      return { ...state, signInSucceed: false, connectedUser: action.payload };
    }
    case "sign-in/attempt/error": {
      return { ...state, signInError: action.payload };
    }

    /* TODO use toast */
    case "register/attempt/success/signal": {
      return { ...state, registerSucceed: true, registerError: null };
    }
    case "register/attempt/error": {
      return {
        ...state,
        registerSucceed: false,
        registerError: action.payload
      };
    }

    case "sign-out/attempt/success/signal": {
      return { ...state, signOutSucceed: true };
    }
    case "sign-out/attempt/success": {
      return { ...state, connectedUser: null };
    }

    /* TODO use toast */
    case "profile/update/success": {
      const connectedUser = {
        ...state.connectedUser,
        ...action.payload
      };
      return { ...state, connectedUser, profileUpdateSucceed: true };
    }
    case "profile/update/error": {
      return { ...state, profileUpdateError: action.payload };
    }

    /* TODO use toast */
    case "profile/load/success": {
      return { ...state, profileSelectedUser: action.payload };
    }

    case "profile/load/error": {
      return { ...state, profileLoadError: action.payload };
    }

    case "posts/follow/success": {
      const updatedConnectedUser = action.payload.following
        ? {
            ...state.connectedUser,
            following: []
              .concat(state.connectedUser.following.split(","), [
                action.payload.userId
              ])
              .filter(str => str !== "")
              .join(",")
          }
        : {
            ...state.connectedUser,
            following: state.connectedUser.following
              .split(",")
              .filter(userId => userId !== action.payload.userId)
              .join(",")
          };
      return { ...state, connectedUser: updatedConnectedUser };
    }

    case "search/search/success": {
      if (
        action.payload.search.method === "posts" ||
        action.payload.search.method === "hashtags"
      ) {
        return { ...state, posts: action.payload.result };
      } else if (action.payload.search.method == "profile") {
        return { ...state, searchResults: action.payload.result };
      }
    }

    default:
      return state;
  }
});

export * from "./actions";
