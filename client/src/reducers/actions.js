import { koos } from "../services/koos";
import { hash } from "../utils";

/* Actions */

export const PageLoadSuccess = () => ({
  type: "index/load/success",
  payload: null
});

export const PageLoadError = error => ({
  type: "index/load/error",
  payload: error
});

export const PostsPageLoadPostsSuccess = posts => ({
  type: "posts/posts/load/success",
  payload: posts
});

export const PostsPageLoadPostsError = error => ({
  type: "posts/posts/load/error",
  payload: error
});

export const PostsPagePostSubmitSuccess = post => ({
  type: "posts/post/success",
  payload: post
});
export const PostsPagePostSubmitError = error => ({
  type: "posts/post/error",
  payload: error
});

export const PostsPageCommentSubmitSuccess = data => ({
  type: "posts/comment/success",
  payload: data
});
export const PostsPageCommentSubmitError = error => ({
  type: "posts/comment/error",
  payload: error
});

export const SignInPageAttemptSuccessSignal = answer => ({
  type: "sign-in/attempt/success/signal",
  payload: answer
});

export const SignInPageAttemptSuccess = answer => ({
  type: "sign-in/attempt/success",
  payload: answer
});

export const SignInPageAttemptError = error => ({
  type: "sign-in/attempt/error",
  payload: error
});

export const RegisterPageAttemptSuccessSignal = answer => ({
  type: "register/attempt/success/signal",
  payload: answer
});

export const RegisterPageAttemptSuccess = answer => ({
  type: "register/attempt/success",
  payload: answer
});

export const RegisterPageAttemptError = error => ({
  type: "register/attempt/error",
  payload: error
});

export const SignOutPageAttemptSucceedSignal = () => ({
  type: "sign-out/attempt/succeed/signal"
});

export const SignOutPageAttemptSuccess = () => ({
  type: "sign-out/attempt/success"
});

export const ProfilePageUpdateSuccess = data => ({
  type: "profile/update/success",
  payload: data
});

export const ProfilePageUpdateError = error => ({
  type: "profile/update/error",
  payload: error
});

export const ProfilePageLoadSuccess = data => ({
  type: "profile/load/success",
  payload: data
});

export const ProfilePageLoadError = error => ({
  type: "profile/load/error",
  payload: error
});

export const SearchPageSearchSuccess = data => ({
  type: "search/search/success",
  payload: data
});

export const SearchPageSearchError = error => ({
  type: "search/search/error",
  payload: error
});

export const PostsPageFollowUpdateSuccess = data => ({
  type: "posts/follow/success",
  payload: data
});

export const PostsPageFollowUpdateError = error => ({
  type: "posts/follow/error",
  payload: error
});

/* Thunks */

export const PageLoad = () => dispatch => {
  console.info("Initiate page load");
  return (
    koos
      /*
       * TODO: rewrite so it doens't fetch the user at every page
       * Right now the connectedUser is lost at every reload
       * See issue #29
       */
      .init()
      .then(() => dispatch(PageLoadSuccess()))
      .catch(error => {
        console.error(error);
        dispatch(PageLoadError(error));
      })
  );
};

export const PostsPageLoadPosts = userId => dispatch => {
  console.info("Initiate Posts load");
  koos
    .loadPosts(userId)
    .then(posts => {
      console.log("trying to dispatch success", posts);
      dispatch(PostsPageLoadPostsSuccess(posts));
    })
    .catch(error => {
      console.error(error);
      dispatch(PostsPageLoadPostsError(error));
    });
};

export const PostsPagePostSubmit = post => dispatch =>
  koos
    .addPost(post)
    .then(({ post, answer }) => {
      console.log("dispatching post success", post);
      dispatch(PostsPagePostSubmitSuccess(post));
    })
    .catch(error => {
      console.error(error);
      dispatch(PostsPagePostSubmitError(error));
    });

export const PostsPageCommentSubmit = data => dispatch =>
  koos
    .addComment(data)
    .then(response => {
      console.log("dispatching comment success", response);
      dispatch(PostsPageCommentSubmitSuccess(data));
    })
    .catch(error => {
      console.error(error);
      dispatch(PostsPageCommentSubmitError(error));
    });

/*
 * TODO: check if this doesn't put too much charge on the server
 * implement one-shot signing if needed
 */
export const SignInPageAttempt = data => dispatch =>
  koos
    .signIn(data)
    .then(answer => {
      console.log("dispatching sign in attempt success signal", answer);
      dispatch(SignInPageAttemptSuccessSignal(answer.user.name));
      setTimeout(() => {
        dispatch(SignInPageAttemptSuccess(answer.user));
      }, 3000);
    })
    .catch(error => {
      console.error(error);
      dispatch(SignInPageAttemptError(error));
    });

export const RegisterPageAttempt = data => dispatch =>
  koos
    .register({ ...data, userId: hash(data) })
    .then(({ answer }) => {
      console.log("dispatching register attempt success");
      if (answer.answer === 409)
        throw Error("This username or email is already taken");
      if (answer.answer !== 201) throw Error("Something went wrong");
      dispatch(RegisterPageAttemptSuccessSignal(answer));
      setTimeout(() => {
        const { email, password } = data;
        dispatch(SignInPageAttempt({ email, password }));
      }, 3000);
    })
    .catch(error => {
      console.error(error);
      dispatch(RegisterPageAttemptError(error));
    });

export const SignOutPageAttempt = () => dispatch =>
  koos.signOut().then(() => {
    console.log("dispatching sign out attempt success signal");
    dispatch(SignOutPageAttemptSucceedSignal());
    setTimeout(() => {
      console.log("dispatching sign out attempt success");
      dispatch(SignOutPageAttemptSuccess());
    }, 3000);
  });

export const ProfilePageUpdate = data => dispatch =>
  koos
    .updateProfile(data)
    .then(() => {
      console.log("dispatching profile update success");
      dispatch(ProfilePageUpdateSuccess(data));
    })
    .catch(error => {
      console.error(error);
      dispatch(ProfilePageUpdateError(error));
    });

export const ProfilePageLoad = userId => dispatch =>
  koos
    .getProfile(userId)
    .then(({ user }) => {
      console.log("dispatching profile page load");
      dispatch(ProfilePageLoadSuccess(user));
    })
    .catch(error => {
      dispatch(ProfilePageLoadError(error));
    });

export const SearchPageSearch = search => dispatch =>
  koos
    .search(search)
    .then(result => {
      console.log("dispatching search page result");
      dispatch(SearchPageSearchSuccess({ search, result }));
    })
    .catch(error => {
      dispatch(SearchPageSearchError(error));
    });

export const PostsPageFollowUpdate = update => dispatch =>
  koos
    .followUpdate(update)
    .then(answer => {
      console.log("dispatching post page follow update success");
      dispatch(PostsPageFollowUpdateSuccess(update));
    })
    .catch(error => {
      dispatch(PostsPageFollowUpdateError(error));
    });
