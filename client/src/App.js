import React from "react";

import { Provider } from "react-redux";
import "./App.css";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { reducer } from "./reducers";

import {
  AboutPage,
  LandingPage,
  PostsPage,
  ProfilePage,
  ProjectPage,
  RegisterPage,
  SearchPage,
  SettingsPage,
  SignInPage,
  SignOutPage
} from "./containers";

import { Route, Switch, HashRouter } from "react-router-dom";

const store = createStore(reducer, applyMiddleware(thunk));

function App() {
  return (
    <HashRouter>
      <Provider store={store}>
        <Switch>
          <Route
            path="/profile/:userId"
            render={props => <ProfilePage {...props} />}
          />
          <Route
            path="/posts/:userId"
            render={props => <PostsPage {...props} />}
          />
          <Route path="/posts" render={props => <PostsPage {...props} />} />
          <Route
            path="/register"
            render={props => <RegisterPage {...props} />}
          />
          <Route
            path="/search/:method/:target"
            render={props => <SearchPage {...props} />}
          />
          <Route
            path="/settings"
            render={props => <SettingsPage {...props} />}
          />
          <Route path="/sign-in" render={props => <SignInPage {...props} />} />
          <Route
            path="/profile"
            exact
            render={props => <ProfilePage {...props} />}
          />
          <Route
            path="/sign-out"
            exact
            render={props => <SignOutPage {...props} />}
          />
          <Route
            path="/project"
            exact
            render={props => <ProjectPage {...props} />}
          />
          <Route
            path="/about"
            exact
            render={props => <AboutPage {...props} />}
          />
          <Route path="/" exact render={props => <LandingPage {...props} />} />
        </Switch>
      </Provider>
    </HashRouter>
  );
}

export default App;
