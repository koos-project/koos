# Contributing to Koos

## Introduction

Hi there. Thank you for your interest in our little project. Koos is
a decentralized social media built on (Fluence)[http://fluence.network]. 
It is produced and supported by the 
[Chatnoir Digital DAO](http://chatnoir.digital). Don't hesitate to hit us up on 
our [riot.im](https://riot.im/app/#/room/!yKRGJWopsGxpcftwiU:matrix.org).

## How to contribute

We are looking for people interested in helping us! We especially needs one of 
those skills:

- React
- Webassembly / assemblyscript (if you can do some Typescript and node, you'd be
    great)
- Graphic design / CSS - SASS.
- UX

But you could also help by user testing, documenting, giving us insights and idea and so on.

You can pick one issue on our board and start working, we will gladly review 
your PR! We in crucial needs of unit-test and end-to end testing. 

