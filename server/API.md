# getPosts

## request:
```
{
    "action": "getPosts",

    // OPTIONAL STRING. one string with one hashtag to look for posts that includes them, DONT include the #
    "hashtag": "HappyDay",
    
    // STRING, user UUID
    "userID": "ifajoer34ifov43892u3nf93f8dsfasdfa2321",

    // INTEGER. skip first `skip` posts
    "skip": 5,

    // INTEGER. return no more than `limit` of posts
    "limit": 20
    
    // STRING, user UUID
    "userId": "sadofhjwuheuo23980hg9r8fqwuhofasDFWAJEF"
}
```
## response
```
{
    "action": "getPosts", 
    
    // INTEGER
    "answer": 200,

    "posts": [
        {
            // STRING, user UUID
            "userId": "sadofhjwuheuo23980hg9r8fqwuhofasDFWAJEF"

            // STRING
            "content": "ShitPost things",

            // INTEGER. epoch time
            "time": 234792348,

            // STRING. post uuid
            "postId": "sadofhjwuheuo23980hg9r8fqwuhofasDFWAJEF",

            // STRING. one or multiple hashtags seperated by comma, NO spaces, 
            // can be empty string but 'hashtags' must be in the request
            "hashtags": "SomeHashtag,SomeOtherHashtag,ThisIsHashtagToo,MeToo:D",
            
            // ARRAY <commentUUID>
            comments: [
                "sadofhjwuheuo23980hg9r8fqwuhofasDFWAJEF".
                "i32923dfdf8fdsa943fdigj83fdsafbbvc8fdag",
                "fj3d321fgih8fsaDFfg0dERfrejiFIEFfdsadsa"
            ]
        }, 
        {
            // ^^^^ the same as above
        }
    ]
}
```
### or
```
{
    "action": "getPosts",

    // INTEGER
    "answer": 404,

    "message": "error message here"
}
```

# addPost
## request:
```
{
    "action": "addPost",

    // STRING. uuid of the post
    "postId": "ieourbhofhqwue9r28389823rsudasDFQWehuof",
    
    // STRING
    "content": "Creeper? Aww man!",

    // STRING. you get it by signIn
    "token": "234",

    // INTEGER. time of post creation stored as epoch time integer
    "time": 237497264,

    // STRING. one or multiple hashtags seperated by comma, NO spaces, 
    // can be empty string but 'hashtags' must be in the request
    "hashtags": "SomeHashtag,SomeOtherHashtag,ThisIsHashtagToo,MeToo:D"
}
```
## response
```
{
    "action": "addPost", 
    
    // INTEGER
    "answer": 200
}
```
### or
```
{
    "action": "addPost",

    // INTEGER
    "answer": 503,

    "message": "error message here"
}
```


# getUser
## request
```
{
    "action": "getUser",

    // STRING. user uuid
    "userId": "dhsfoauasdjfhouhai3452342hsdfwqr8uwer9qwef"
}
```
## response
```
{
    "action": "getUser", 
    
    // INTEGER
    "answer": 200,

    // STRING. user name
    'name': 'itsMe_Marioo',

    // STRING
    'email': 'mario@nintendo.com',
    
    // STRING
    'presentation': 'Hello world! I\'m Mario. Please give me coins!',

    // BASE64 STRING. empty if no picture
    'avatar': 'asodhfwaeuuhf2398h2f9ufoqwqefsdaFWAEFQWEAFhquwef8uqwef',
    
    // STRING
    'presentation': "Hello world!"'
}
```
### or
```
{
    "action": "getUser",

    // INTEGER
    "answer": 404,

    "message": "error message here"
}
```

# addUser
## request
```
{
    'action': 'addUser',

    // STRING
    'name': 'userNameWithNoSpaces',

    // STRING
    'email': 'mario@nintendo.com',

    // STRING (SHA)
    'password': '2938rhoweuf',
    
    // STRING
    'presentation': 'Hello world! I am Mario. I\'m there for long',

    // BASE64 STRING. empty if no picture
    'avatar': 'ashioufowq3424p23423eufohuqfADSFQWEFHDSFwrwef',

    // STRING
    'userId': 'asefhiqoweuf0834ru2'
}
```
## response
```
{
    "action": "addUser", 
    
    // INTEGER
    "answer": 200
}
```
### or
```
{
    "action": "addUser",

    // INTEGER
    "answer": 404,

    "message": "error message here"
}
```

# updateUser
## request
```
{
    'action': 'updateUser',

    // STRING (optional)
    'name': 'userNameWithNoSpaces',

    // STRING (optional)
    'password': '2938rhoweuf',

    // BASE64 STRING. empty if no picture  (optional)
    'avatar': 'ashioufowq3424p23423eufohuqfADSFQWEFHDSFwrwef',

    // STRING (mandatory)
    'userId': 'asefhiqoweuf0834ru2'
}
```
## response
```
{
    "action": "updateUser", 
    
    // INTEGER
    "answer": 200
}
```
### or
```
{
    "action": "updateUser",

    // INTEGER
    "answer": 404,

    "message": "error message here"
}
```

# getMe

![Checked][Checked] This schema have been double-checked on the frontend side

## request
```
{
    'action': 'getMe',

    // STRING. you get it by signIn
    'token': '2342'
}
```
## same as getUser

# signIn
![Checked][Checked] This schema have been double-checked on the frontend side

## request 
```
{
    'action': 'signIn',

    'email': 'aoehf@ahsoduf.com',

    'password': 'ajshoufhw324089wefho'
}
```
## response
```
{
    "action": "signIn", 
    
    // INTEGER
    "answer": 200,

    // STRING. use it for other requests that requires validation
    'token': '2342'
}
```
### or 
```
{
    'action': 'signIn',

    // INTEGER
    'answer': 401,

    'message': 'Wrong password'
}
```
### or 
```
{
    'action': 'signIn',

    // INTEGER
    'answer': 404,

    'message': 'No user with such email'
}
```

# addComment
## request
```
{
    'action': 'addComment',

    // STRING. COMMENT uuid
    'commentId': 'ASFhoweafohw98fhouq39fadfs',

    // STRING. you get it by signIn
    'token': '32423',

    // STRING
    'postId': 'woeuhfosau2843yrh',

    // STRING
    'content': 'ajoshfo',

    // INTEGER. epoch time
    'time': 234678237
}
```
## response
```
{
    "action": "addComment", 
    
    // INTEGER
    "answer": 200,
}
```
### or 
```
{
    "action": "addComment",

    // INTEGER
    "answer": 503,

    "message": "error message here"
}
```
### or (no valid token)
```
{
    'action': 'addComment',

    // INTEGER
    'answer': 401,

    'message': 'unauthorized'
}
```

# getPostComments
## request 
```
{
    'action': 'getPostComments',

    'postId': "jfoqwheruhfosihf9woehq9f8ewhf9qw8ehufwq98pefu9",
    'token': '324'
}
```
## response
```
{
    'action': 'getPostComments',

    // INTEGER
    "answer": 200,

    'comments': [
        {
            // STRING, user UUID
            "userId": "jfoqwheruhfosihf9woehq9f8ewhf9qw8ehufwq98pefu9",

            // STRING
            "content": "Nice!",

            // INTEGER. epoch time
            "time": 234792348,

            // STRING. comment uuid
            "commentId": "sadofhjwuheuo23980hg9r8fqwuhofasDFWAJEF",
        },
        {
            // ^^^^ the same as above
        },
    ]
}
```
### or
```
{
    "action": "getPostComments",

    // INTEGER
    "answer": 404,

    "message": "this comment doesnt exist"
}
```
### or (no valid token)
```
{
    'action': 'getPostComments',

    // INTEGER
    'answer': 401,

    'message': 'unauthorized'
}
```

[Checked]: https://cdn2.iconfinder.com/data/icons/flat-icons-web/40/Edit-256.png