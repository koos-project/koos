This is the Koos server. This is handling authentification and
access control, and communicate with the client through fluent
on one side and with Redis/LiteSQL on the other. It is written 
in [AssemblyScript](https://docs.assemblyscript.org/). 

See also: https://fluence.dev/docs/quick-backend

Usage
----

`yarn install`
Install dependancies

`yarn run asbuild`
Compile to WASM
