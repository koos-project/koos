/*
 *
 *   __  ___   ______     ______        _______.
 *  |  |/  /  /  __  \   /  __  \      /       |
 *  |  '  /  |  |  |  | |  |  |  |    |   (----`
 *  |    <   |  |  |  | |  |  |  |     \   \
 *  |  .  \  |  `--'  | |  `--'  | .----)   |
 *  |__|\__\  \______/   \______/  |_______/
 *
 **/
import {
  JSONDecoder,
  JSONHandler
} from "../node_modules/assemblyscript-json/assembly/decoder";
import { JSONEncoder } from "../node_modules/assemblyscript-json/assembly/encoder";
import { log } from "../node_modules/assemblyscript-sdk/assembly/logger";

import { Comment, Post, User } from "./models";
import {
  addComment,
  addPost,
  addUser,
  getComments,
  getFollowing,
  getPostIDByUUID,
  getUser,
  getUserIDByLogin,
  getUserByName,
  getUserPosts,
  postExists,
  getPostByContent,
  getPostByHashtag,
  updateUser,
  updateFollow
} from "./database";

export abstract class Request {
  respond(): string {
    unreachable();
    return "{}";
  }

  clean(): void {}
}

export class GetPostsRequest extends Request {
  constructor(
    public token: string,
    // public hashtag: string,
    // public skip: u32,
    // public limit: u32,
    public userId: string
  ) {
    super();
  }

  respond(): string {
    // let posts = getUserPosts(this.limit, this.skip, this.hashtag, this.userId);
    let posts = getUserPosts(this.userId);
    let responseStr: string;

    // let users = getPostsAuthors(posts);
    responseStr = this.serialize(posts);

    memory.free(changetype<usize>(posts));

    return responseStr;
  }

  private serialize(posts: Array<Post>): string {
    let encoder = new JSONEncoder();
    encoder.pushObject(null);

    encoder.setString("action", "getPosts");
    encoder.setInteger("answer", 200);

    encoder.pushArray("posts");
    for (let i = 0; i < posts.length; i++) {
      encoder.pushObject(null);

      encoder.setString("userId", posts[i].userId);

      encoder.setString("content", posts[i].content);
      encoder.setInteger("timestamp", posts[i].timestamp);
      encoder.setString("postId", posts[i].postId);
      encoder.setString("hashtags", posts[i].hashtags);

      encoder.popObject();
    }
    encoder.popArray();

    encoder.popObject();
    return encoder.toString();
  }

  clean(): void {
    super.clean();
    memory.free(changetype<usize>(this.token));
    // memory.free(changetype<usize>(this.skip));
    // memory.free(changetype<usize>(this.limit));
  }
}

export class AddPostRequest extends Request {
  constructor(
    public readonly postId: string,
    public userId: string,
    public content: string,
    public timestamp: u64,
    public hashtags: string
  ) {
    super();
  }

  private serialize(): string {
    let encoder = new JSONEncoder();
    encoder.pushObject(null);

    encoder.setString("action", "addPost");
    encoder.setInteger("answer", 200);

    encoder.popObject();
    return encoder.toString();
  }

  clean(): void {
    super.clean();
    memory.free(changetype<usize>(this.postId));
    memory.free(changetype<usize>(this.content));
  }

  respond(): string {
    let post = new Post(
      this.postId,
      this.userId,
      this.content,
      this.timestamp,
      this.hashtags
    );
    let success = addPost(post);
    memory.free(changetype<usize>(this.postId));
    memory.free(changetype<usize>(this.userId));
    memory.free(changetype<usize>(this.content));

    let responseStr: string;

    if (success) {
      responseStr = this.serialize();
    } else {
      let response = new ErrorResponse(
        503,
        "couldnt add post, maybe uuid does exist? or invalid user token?",
        "addPost"
      );
      responseStr = response.serialize();
      memory.free(changetype<usize>(response));
    }

    return responseStr;
  }
}

export class GetUserRequest extends Request {
  constructor(public userId: string) {
    super();
  }

  respond(): string {
    let user = getUser(this.userId);
    let responseStr: string;

    if (user == null) {
      let response = new ErrorResponse(404, "user doesnt exist", "getUser");
      responseStr = response.serialize();
      memory.free(changetype<usize>(response));
    } else {
      let following: Array<string> = getFollowing(user.userId);

      responseStr = this.serialize(
        user.userId,
        user.name,
        user.email,
        user.password,
        user.presentation,
        user.avatar,
        following
      );
    }

    memory.free(changetype<usize>(user));

    return responseStr;
  }

  private serialize(
    userId: string,
    name: string,
    email: string,
    password: string,
    presentation: string,
    avatar: string,
    following: Array<string>
  ): string {
    let encoder = new JSONEncoder();
    encoder.pushObject(null);
    encoder.setString("action", "getUser");
    encoder.setInteger("answer", 200);

    encoder.pushObject("user");
    encoder.setString("userId", userId);
    encoder.setString("name", name);
    encoder.setString("email", email);
    encoder.setString("password", password);
    encoder.setString("presentation", presentation);
    encoder.setString("avatar", avatar);
    encoder.setString("following", following.join(","));

    encoder.popObject();

    encoder.popObject();
    return encoder.toString();
  }

  clean(): void {
    super.clean();
    memory.free(changetype<usize>(this.userId));
  }
}

export class AddUserRequest extends Request {
  constructor(
    public userId: string,
    public name: string,
    public email: string,
    public password: string,
    public presentation: string
  ) {
    super();
  }

  respond(): string {
    log("--- [ADDUSERREQUEST] respond()");
    let user = new User(
      this.userId,
      this.name,
      this.email,
      this.password,
      this.presentation,
      null
    );

    log("--- [ADDUSERREQUEST] created new user()");
    let addUserResponse: number = addUser(user);
    log("--- [ADDUSERREQUEST] added new user");

    let responseStr: string;
    if (addUserResponse == 201) {
      responseStr = this.serialize();
    } else if (addUserResponse == 409) {
      // TODO clarify if the conflict is on email or name
      let response = new ErrorResponse(
        409,
        "Please chose another name or email",
        "addUser"
      );
      responseStr = response.serialize();
      memory.free(changetype<usize>(response));
    } else if (addUserResponse == 500) {
      let response = new ErrorResponse(500, "Could not add user", "addUser");
      responseStr = response.serialize();
      memory.free(changetype<usize>(response));
    }

    memory.free(changetype<usize>(user));

    return responseStr;
  }

  private serialize(): string {
    let encoder = new JSONEncoder();
    encoder.pushObject(null);

    encoder.setString("action", "addUser");
    encoder.setInteger("answer", 201);

    encoder.popObject();
    return encoder.toString();
  }

  clean(): void {
    super.clean();
    memory.free(changetype<usize>(this.userId));
    memory.free(changetype<usize>(this.name));
    memory.free(changetype<usize>(this.email));
    memory.free(changetype<usize>(this.password));
    memory.free(changetype<usize>(this.presentation));
  }
}

export class UpdateUserRequest extends Request {
  constructor(
    public userId: string,
    public token: string,
    public presentation: string,
    public password: string,
    public avatar: string
  ) {
    super();
  }

  respond(): string {
    log("[UpdateUserRequest]");
    log(this.avatar);
    let user = new User(
      this.userId,
      null,
      null,
      this.password,
      this.presentation,
      this.avatar
    );
    let responseStr: string;

    if (updateUser(user)) {
      responseStr = this.serialize();
    } else {
      let response = new ErrorResponse(
        404,
        "couldnt update user",
        "updateUser"
      );
      responseStr = response.serialize();
      memory.free(changetype<usize>(response));
    }

    memory.free(changetype<usize>(user));

    return responseStr;
  }

  private serialize(): string {
    let encoder = new JSONEncoder();
    encoder.pushObject(null);

    encoder.setString("action", "updateUser");
    encoder.setInteger("answer", 200);

    encoder.popObject();
    return encoder.toString();
  }

  clean(): void {
    super.clean();
    memory.free(changetype<usize>(this.userId));
    memory.free(changetype<usize>(this.token));
    memory.free(changetype<usize>(this.password));
    memory.free(changetype<usize>(this.presentation));
  }
}

export class GetMeRequest extends Request {
  constructor(public token: string) {
    super();
  }

  respond(): string {
    let responseStr: string;

    let user = getUser(this.token);

    if (user == null) {
      let response = new ErrorResponse(
        404,
        "This token does not exists",
        "getMe"
      );
      responseStr = response.serialize();
      memory.free(changetype<usize>(response));
    } else {
      let following: Array<string> = getFollowing(user.userId);
      responseStr = this.serialize(
        user.userId,
        user.name,
        user.email,
        user.password,
        user.presentation,
        user.avatar,
        following
      );
    }

    memory.free(changetype<usize>(user));

    return responseStr;
  }

  private serialize(
    userId: string,
    name: string,
    email: string,
    password: string,
    presentation: string,
    avatar: string,
    following: Array<string>
  ): string {
    let encoder = new JSONEncoder();
    encoder.pushObject(null);

    encoder.setString("action", "getMe");
    encoder.setInteger("answer", 200);

    encoder.pushObject("user");
    encoder.setString("userId", userId);
    encoder.setString("name", name);
    encoder.setString("email", email);
    encoder.setString("password", password);
    encoder.setString("presentation", presentation);
    encoder.setString("avatar", avatar);
    encoder.setString("following", following.join(","));
    encoder.popObject();

    encoder.popObject();
    return encoder.toString();
  }

  clean(): void {
    super.clean();
    memory.free(changetype<usize>(this.token));
  }
}

export class SignInRequest extends Request {
  constructor(public email: string, public password: string) {
    super();
  }

  respond(): string {
    let userId = getUserIDByLogin(this.email, this.password);
    let responseStr: string;

    log("[SignInRequest - respond] - id:");
    log(userId);

    if (userId == "wrong_email") {
      let response = new ErrorResponse(404, "Wrong Email", "signIn");
      responseStr = response.serialize();
      memory.free(changetype<usize>(response));
    } else if (userId == "wrong_password") {
      let response = new ErrorResponse(401, "Wrong Password", "signIn");
      responseStr = response.serialize();
      memory.free(changetype<usize>(response));
    } else {
      responseStr = this.serialize(userId);
      /* TODO: generate a private token to return to the server.
       * For now the userId is enough
       */
    }

    return responseStr;
  }

  private serialize(token: string): string {
    let encoder = new JSONEncoder();
    encoder.pushObject(null);

    encoder.setString("action", "signIn");
    encoder.setInteger("answer", 200);

    encoder.setString("token", token);

    encoder.popObject();
    return encoder.toString();
  }

  clean(): void {
    super.clean();
    memory.free(changetype<usize>(this.email));
    memory.free(changetype<usize>(this.password));
  }
}

export class AddCommentRequest extends Request {
  constructor(
    public readonly commentId: string,
    public postId: string,
    public userId: string,
    public content: string,
    public timestamp: u64
  ) {
    super();
  }

  respond(): string {
    let comment = new Comment(
      this.commentId,
      this.postId,
      this.userId,
      this.content,
      this.timestamp
    );

    let success = addComment(comment);
    memory.free(changetype<usize>(comment));

    let responseStr: string;

    if (success) {
      responseStr = this.serialize();
    } else {
      let response = new ErrorResponse(
        503,
        "couldnt add comment, maybe uuid does exist? or invalid user token?",
        "addComment"
      );
      responseStr = response.serialize();
      memory.free(changetype<usize>(response));
    }

    return responseStr;
  }

  private serialize(): string {
    let encoder = new JSONEncoder();
    encoder.pushObject(null);

    encoder.setString("action", "addComment");
    encoder.setInteger("answer", 200);

    encoder.popObject();
    return encoder.toString();
  }

  clean(): void {
    super.clean();
  }
}

export class GetCommentsRequest extends Request {
  constructor(public postId: string, public token: string) {
    super();
  }

  respond(): string {
    log("[GetCommentRequest.respond()] postId");
    log(this.postId);

    if (!postExists(this.postId)) {
      let response = new ErrorResponse(404, "Post doesnt exist", "getComments");
      let responseStr = response.serialize();
      memory.free(changetype<usize>(response));
      return responseStr;
    }

    let comments: Array<Comment> = getComments(this.postId);
    let responseStr: string = this.serialize(comments);

    memory.free(changetype<usize>(comments));

    return responseStr;
  }

  private serialize(comments: Array<Comment>): string {
    let length: number = comments.length;
    log(length.toString());

    let encoder = new JSONEncoder();
    encoder.pushObject(null);

    encoder.setString("action", "getComments");
    encoder.setInteger("answer", 200);

    encoder.pushArray("comments");
    for (let i = 0; i < comments.length; i++) {
      encoder.pushObject(null);

      encoder.setString("commentId", comments[i].commentId);
      encoder.setString("postId", comments[i].postId);
      encoder.setString("userId", comments[i].userId);
      encoder.setString("content", comments[i].content);
      encoder.setInteger("timestamp", comments[i].timestamp);

      encoder.popObject();
    }
    encoder.popArray();

    encoder.popObject();
    return encoder.toString();
  }

  clean(): void {
    super.clean();
  }
}

export class SearchRequest extends Request {
  constructor(
    public token: string,
    public method: string,
    public target: string
  ) {
    super();
  }

  respond(): string {
    let responseStr: string = "{}";

    if (this.target == "") {
      let response = new ErrorResponse(404, "empty target", "search");
      let responseStr = response.serialize();
      memory.free(changetype<usize>(response));
    } else if (this.method == "posts") {
      let posts = getPostByContent(this.target);
      responseStr = this.serializePosts(posts);
    } else if (this.method == "profile") {
      let users = getUserByName(this.target);
      responseStr = this.serializeUsers(users);
    } else if (this.method == "hashtag") {
      let posts = getPostByContent(this.target);
      responseStr = this.serializePosts(posts);
    } else {
      let response = new ErrorResponse(400, "invalid method", "search");
      let responseStr = response.serialize();
      memory.free(changetype<usize>(response));
    }

    log("[SearchRequest - respond()]");
    log(responseStr);
    return responseStr;
  }

  private serializeUsers(users: Array<User>): string {
    let encoder = new JSONEncoder();
    encoder.pushObject(null);

    encoder.setString("action", "search");
    encoder.setInteger("answer", 200);

    encoder.pushArray("users");
    for (let i = 0; i < users.length; i++) {
      encoder.pushObject(null);
      encoder.setString("userId", users[i].userId);
      encoder.setString("name", users[i].name);
      encoder.setString("email", users[i].email);
      encoder.setString("password", users[i].password);
      encoder.setString("presentation", users[i].presentation);
      encoder.setString("avatar", users[i].avatar);
      encoder.popObject();
    }
    encoder.popArray();

    encoder.popObject();
    return encoder.toString();
  }

  private serializePosts(posts: Array<Post>): string {
    log("serializing Posts");
    let encoder = new JSONEncoder();
    encoder.pushObject(null);

    encoder.setString("action", "search");
    encoder.setInteger("answer", 200);

    let postLength = posts.length;
    log("postLength");
    log(postLength.toString());
    log("is it exploding here?");

    encoder.pushArray("posts");
    for (let i = 0; i < posts.length; i++) {
      encoder.pushObject(null);
      log("is it exploding there?");
      log(i.toString());
      log(posts[i].postId);

      encoder.setString("postId", posts[i].postId);
      encoder.setString("userId", posts[i].userId);

      log("is it exploding anywhere?");

      encoder.setString("content", posts[i].content);
      encoder.setInteger("timestamp", posts[i].timestamp);
      encoder.setString("hashtags", posts[i].hashtags);

      log("I AM THE POPE OF DOPE");

      encoder.popObject();
    }
    encoder.popArray();

    encoder.popObject();
    log("Finished serializing posts");
    return encoder.toString();
  }

  clean(): void {
    super.clean();
  }
}

export class UpdateFollowRequest extends Request {
  constructor(
    public token: string,
    public userId: string,
    public targetUserId: string,
    public following: string
  ) {
    super();
  }

  respond(): string {
    log("REQUEST - UPDATE FOLLOW - respond()");
    log(this.following);
    if (getUser(this.userId) == null) {
      let response = new ErrorResponse(
        404,
        "User doesnt exist",
        "updateFollow"
      );
      let responseStr = response.serialize();
      memory.free(changetype<usize>(response));
      return responseStr;
    } else if (!updateFollow(this.userId, this.targetUserId, this.following)) {
      let response = new ErrorResponse(
        500,
        "Something went wrong",
        "updateFollow"
      );
      let responseStr = response.serialize();
      memory.free(changetype<usize>(response));
      return responseStr;
    } else {
      return this.serialize();
    }
  }

  private serialize(): string {
    let encoder = new JSONEncoder();
    encoder.pushObject(null);

    encoder.setString("action", "updateFollow");
    encoder.setInteger("answer", 200);

    encoder.popObject();
    return encoder.toString();
  }

  clean(): void {
    super.clean();
  }
}

export class ErrorResponse {
  constructor(
    private code: u16,
    private message: string,
    private action: string
  ) {}

  serialize(): string {
    let encoder = new JSONEncoder();

    encoder.pushObject(null);
    encoder.setString("action", this.action);
    encoder.setInteger("answer", this.code);
    encoder.setString("message", this.message);
    encoder.popObject();

    return encoder.toString();
  }
}
