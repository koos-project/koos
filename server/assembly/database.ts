/*
 *
 *   __  ___   ______     ______        _______.
 *  |  |/  /  /  __  \   /  __  \      /       |
 *  |  '  /  |  |  |  | |  |  |  |    |   (----`
 *  |    <   |  |  |  | |  |  |  |     \   \
 *  |  .  \  |  `--'  | |  `--'  | .----)   |
 *  |__|\__\  \______/   \______/  |_______/
 *
 **/
import { log } from "../node_modules/assemblyscript-sdk/assembly/logger";
import { query } from "../node_modules/db-connector/assembly/index";

import { split, dumpArray } from "./split";
import { Post, User, Comment } from "./models";

export function doRequest(request: string): string {
  log("[doRequest] Request: " + request);
  let result = query(request);
  log("[doRequest] Result:" + result);
  return result;
}

function isOK(response: string): bool {
  return response.length >= 2 && response.substr(0, 2) == "OK";
}

function isUnique(response: string): bool {
  return response.length >= 6 && response.substr(0, 6) == "UNIQUE";
}

export function getUserIDByName(name: string): u64 {
  let request = "SELECT userId FROM users WHERE name = " + name + ";";
  return Number.parseInt(doRequest(request)) as u64;
}

export function addPost(post: Post): bool {
  let request =
    'INSERT INTO posts VALUES("' +
    post.postId +
    '", "' +
    post.userId +
    '", "' +
    post.content +
    '", ' +
    post.timestamp.toString() +
    ', "' +
    post.hashtags +
    '");';
  let result = doRequest(request);

  if (!isOK(result)) {
    return false;
  }

  for (let i = 0; i < post.hashtags.length; i++) {
    doRequest(
      "INSERT INTO hashtags VALUES(" +
        post.postId +
        "," +
        post.hashtags[i] +
        ");"
    );
  }

  return true;
}

function getHashtags(postId: string): string {
  let request = 'SELECT name FROM hashtags WHERE postId = "' + postId + '"';
  let result = doRequest(request);
  if (isOK(result)) return "";
  else return split(result, "/n").join(",");
}

export function getUserPosts(
  // limit: i32,
  // skip: i32,
  // hashtag: string,
  userId: string
): Array<Post> {
  let userIdWhere = userId ? 'WHERE userId = "' + userId + '"' : "";
  let request =
    "SELECT * FROM posts " +
    // 'SELECT * FROM posts LIMIT ' +
    // limit.toString() +
    // ' OFFSET ' +
    // skip.toString() +
    userIdWhere +
    ";";
  let resultStr = doRequest(request);
  if (isOK(resultStr)) return [];

  let results = split(resultStr, "|");

  let posts = new Array<Post>(results.length);
  for (let i = 0; i < posts.length; i++) {
    let row = split(results[i], ",");
    let postId = row[0];
    let userId = row[1];
    let content = row[2];
    let time = row[3];
    let hashtags = getHashtags(postId);

    posts[i] = new Post(
      postId,
      userId,
      content,
      Number.parseInt(time) as u64,
      hashtags
    );
  }
  return posts;
}

export function getPostIDByUUID(postId: string): string {
  let request = "SELECT postId FROM posts WHERE postId = " + postId + ";";
  return doRequest(request);
}

export function getUser(userId: string): User {
  let request =
    "SELECT userId, name, email, password, presentation, avatar FROM users " +
    'WHERE userId = "' +
    userId +
    '";';
  let resultStr = doRequest(request);

  if (isOK(resultStr)) return null;

  let result = split(resultStr, ",");

  return new User(
    result[0],
    result[1],
    result[2],
    result[3],
    result[4],
    result[5]
  );
}

export function addUser(user: User): number {
  log("--- [DATABASE.tS addUser] inserting user");
  let request =
    'INSERT INTO users VALUES("' +
    user.userId +
    '","' +
    user.name +
    '","' +
    user.email +
    '","' +
    user.password +
    '","' +
    user.presentation +
    '","' +
    "mainpp.png" +
    '");';

  let result = doRequest(request);

  if (isOK(result)) return 201;
  // OK
  else if (isUnique(result)) return 409;
  // CONFLICT
  else return 500; // INTERNAL
}

export function updateUser(user: User): bool {
  log("[updateUser]");
  log(user.userId);
  log(user.avatar);
  log(user.presentation);
  if (user.userId == null || user.userId == "") return false;

  let userIdWhereClause = ' WHERE userId = "' + user.userId + '"';

  if (user.password != null) {
    let request =
      'UPDATE users SET password  = "' +
      user.password +
      '" ' +
      userIdWhereClause +
      ";";
    return isOK(doRequest(request));
  } else if (user.presentation != null) {
    let request =
      'UPDATE users SET presentation  = "' +
      user.presentation +
      '" ' +
      userIdWhereClause +
      ";";
    return isOK(doRequest(request));
  } else if (user.avatar != null) {
    let request =
      'UPDATE users SET avatar = "' +
      user.avatar +
      '" ' +
      userIdWhereClause +
      ";";
    return isOK(doRequest(request));
  } else return false;
}

export function getUserIDByLogin(email: string, password: string): string {
  let emailMatchRequest =
    'SELECT userId FROM users WHERE email = "' + email + '"';
  let emailMatchRequestResponse = doRequest(emailMatchRequest);

  if (isOK(emailMatchRequestResponse)) {
    return "wrong_email";
  }

  let passwordMatchRequest =
    'SELECT userId FROM users WHERE email = "' +
    email +
    '" AND password = "' +
    password +
    '";';
  let passwordMatchRequestResponse = doRequest(passwordMatchRequest);
  if (isOK(passwordMatchRequestResponse)) return "wrong_password";
  return passwordMatchRequestResponse;
}

export function getUserByName(name: string): Array<User> {
  let request =
    'SELECT userId, name, email, password, presentation, avatar FROM users WHERE name LIKE "%' +
    name +
    '%";';
  let resultStr = doRequest(request);

  if (isOK(resultStr)) return new Array();

  let results = resultStr.split("|");
  let users = new Array<User>();

  for (let i = 0; i < results.length; i++) {
    let result = results[i].split(",");
    users.push(
      new User(result[0], result[1], result[2], result[3], result[4], result[5])
    );
  }

  return users;
}

export function getPostByContent(content: string): Array<Post> {
  let request =
    'SELECT postId, userId, content, time, hashtags FROM posts WHERE content LIKE "%' +
    content +
    '%";';
  let resultStr = doRequest(request);

  if (isOK(resultStr)) return new Array();

  let results = resultStr.split("|");
  let posts = new Array<Post>();

  for (let i = 0; i < results.length; i++) {
    let result = results[i].split(",");
    posts.push(
      new Post(
        result[0],
        result[1],
        result[2],
        Number.parseInt(result[3]) as u64,
        result[4]
      )
    );
  }

  return posts;
}

export function getPostByHashtag(hashtag: string): Array<Post> {
  let request =
    'SELECT postId, userId, content, time, hashtags FROM posts CROSS JOIN hashtags ON hashtags.postId = posts.postId WHERE name LIKE "%' +
    hashtag +
    '%";';
  let resultStr = doRequest(request);

  if (isOK(resultStr)) return new Array();

  let results = split(resultStr, "|");
  let posts = new Array<Post>(results.length());

  for (let i = 0; i < results.length; i++) {
    let result = split(results[i], ",");
    posts.push(
      new Post(
        result[0],
        result[1],
        result[2],
        Number.parseInt(result[3]) as u64,
        result[4]
      )
    );
  }

  return posts;
}

export function addComment(comment: Comment): bool {
  let request =
    'INSERT INTO comments VALUES("' +
    comment.commentId +
    '","' +
    comment.userId +
    '","' +
    comment.postId +
    '","' +
    comment.content +
    '","' +
    comment.timestamp.toString() +
    '");';
  let response = doRequest(request);

  return isOK(response);
}

export function postExists(postId: string): boolean {
  let request = 'SELECT COUNT(*) FROM posts WHERE postId = "' + postId + '";';
  return doRequest(request) == "1";
}

export function getComments(postId: string): Array<Comment> {
  doRequest("SELECT * FROM comments");
  let request = 'SELECT * FROM comments WHERE postId = "' + postId + '";';
  let answer = doRequest(request);
  if (isOK(answer)) return new Array();
  let rows = answer.split("|");

  log("RECIEVED ROWS");
  let length: number = rows.length;
  log(length.toString());

  let comments = new Array<Comment>();
  for (let i = 0; i < rows.length; i++) {
    log("\\e[7m LOOPING THROUGH ROWS \\e[27m");
    log(i.toString());
    log(rows[i]);

    let row = split(rows[i], ",");

    if (row.length != 5) {
      log("SQLITE answered a wrong number of columns");
      log(rows[i]);
      continue;
    }

    comments.push(
      new Comment(row[0], row[1], row[2], row[3], Number.parseInt(
        row[4]
      ) as u64)
    );
  }
  return comments;
}

export function updateFollow(
  userId: string,
  targetUserId: string,
  following: string
): boolean {
  log("[DATABASE] - updateFollow");
  log(targetUserId);
  log(following);
  if (following == "true") {
    return isOK(
      doRequest(
        'INSERT INTO follow VALUES("' + userId + '", "' + targetUserId + '")'
      )
    );
  } else {
    return isOK(
      doRequest(
        'DELETE FROM follow WHERE userId = "' +
          userId +
          '" AND targetUserId = "' +
          targetUserId +
          '"'
      )
    );
  }
}

export function getFollowing(userId: string): Array<string> {
  let result = doRequest(
    'SELECT targetUserId FROM follow WHERE userId = "' + userId + '"'
  );
  log("getFollowing: ");
  log(result);
  if (isOK(result)) {
    return [];
  } else if (result.indexOf("|") == -1) {
    return [result];
  } else {
    return result.split("|");
  }
}
