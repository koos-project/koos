/*
 *
 *   __  ___   ______     ______        _______.
 *  |  |/  /  /  __  \   /  __  \      /       |
 *  |  '  /  |  |  |  | |  |  |  |    |   (----`
 *  |    <   |  |  |  | |  |  |  |     \   \
 *  |  .  \  |  `--'  | |  `--'  | .----)   |
 *  |__|\__\  \______/   \______/  |_______/
 *
 **/
export class Post {
  constructor(
    public postId: string,
    public userId: string,
    public content: string,
    public timestamp: u64,
    public hashtags: string
  ) {}
}

export class User {
  constructor(
    public userId: string,
    public name: string,
    public email: string,
    public password: string,
    public presentation: string,
    public avatar: string
  ) {}
}

export class Comment {
  constructor(
    public commentId: string,
    public postId: string,
    public userId: string,
    public content: string,
    public timestamp: u64
  ) {}
}
