/*
 *
 *   __  ___   ______     ______        _______.
 *  |  |/  /  /  __  \   /  __  \      /       |
 *  |  '  /  |  |  |  | |  |  |  |    |   (----`
 *  |    <   |  |  |  | |  |  |  |     \   \
 *  |  .  \  |  `--'  | |  `--'  | .----)   |
 *  |__|\__\  \______/   \______/  |_______/
 *
 **/

import { log } from "../node_modules/assemblyscript-sdk/assembly/logger";
import { doRequest } from "./database";

export function initTables(): void {
  let resp: string;

  log("[initTables] create users: ");
  resp = doRequest(
    "CREATE TABLE IF NOT EXISTS users(" +
      "userId TEXT UNIQUE NOT NULL," +
      "name TEXT UNIQUE NOT NULL," +
      "email TEXT UNIQUE NOT NULL," +
      "password TEXT NOT NULL," +
      "presentation TEXT," +
      "avatar TEXT" +
      ");"
  );
  log("[initTables] response: " + resp);
  memory.free(changetype<usize>(resp));

  log("[initTables] create posts: ");
  resp = doRequest(
    "CREATE TABLE IF NOT EXISTS posts (" +
      "postId TEXT UNIQUE NOT NULL," +
      "userId TEXT NOT NULL " +
      "REFERENCES users(userId) ON UPDATE CASCADE ON DELETE CASCADE," +
      "content TEXT NOT NULL," +
      "time INT NOT NULL," +
      "hashtags TEXT NOT NULL," +
      "PRIMARY KEY (postId)" +
      ");"
  );
  log("[initTables] response: " + resp);
  memory.free(changetype<usize>(resp));

  log("[initTables] create hashtags: ");
  resp = doRequest(
    "CREATE TABLE IF NOT EXISTS hashtags (" +
      "postId INT NOT NULL REFERENCES posts(postId) ON UPDATE CASCADE ON DELETE CASCADE," +
      "name TEXT UNIQUE NOT NULL" +
      ");"
  );
  log("[initTables] response: " + resp);
  memory.free(changetype<usize>(resp));

  log("[initTables] create comments: ");
  resp = doRequest(
    "CREATE TABLE IF NOT EXISTS comments (" +
      "commentId TEXT UNIQUE NOT NULL," +
      "postId INT NOT NULL REFERENCES posts(postId) ON UPDATE CASCADE ON DELETE CASCADE," +
      "userId INT NOT NULL REFERENCES users(userId) ON UPDATE CASCADE ON DELETE CASCADE," +
      "content TEXT NOT NULL," +
      "timestamp INT NOT NULL" +
      ");"
  );
  log("[initTables] response: " + resp);
  memory.free(changetype<usize>(resp));

  log("[initTables] create follow: ");
  resp = doRequest(
    "CREATE TABLE IF NOT EXISTS follow (" +
      "userId INT NOT NULL REFERENCES users(userId) ON UPDATE CASCADE ON DELETE CASCADE," +
      "targetUserId INT NOT NULL REFERENCES users(userId) ON UPDATE CASCADE ON DELETE CASCADE" +
      ");"
  );
  log("[initTables] response: " + resp);
  memory.free(changetype<usize>(resp));
}
