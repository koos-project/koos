import { log } from "../node_modules/assemblyscript-sdk/assembly/logger";

export function split(str: String, sep: String): Array<string> {
  let result: Array<string> = new Array<string>();
  let rest: string = str;

  // For each occurence
  for (let i = str.indexOf(sep); i != -1; i = rest.indexOf(sep)) {
    // Push the beginning of `rest` into `result
    result.push(rest.slice(0, i));
    // Recover the rest of the string
    rest = rest.slice(i + sep.length);
  }

  result.push(rest);
  return result;
}

export function dumpArray(array: Array<string>): void {
  log("--- DUMPARRAY ---");
  for (let i = 0; i < array.length; i++) {
    log("Item " + i.toString());
    log(array[i]);
  }
  log("--- END ---");
}
