/*
 *
 *   __  ___   ______     ______        _______.
 *  |  |/  /  /  __  \   /  __  \      /       |
 *  |  '  /  |  |  |  | |  |  |  |    |   (----`
 *  |    <   |  |  |  | |  |  |  |     \   \
 *  |  .  \  |  `--'  | |  `--'  | .----)   |
 *  |__|\__\  \______/   \______/  |_______/
 *
 **/
import { log } from "../node_modules/assemblyscript-sdk/assembly/logger";

import { decode, RequestProps } from "./decode";
import {
  AddCommentRequest,
  AddPostRequest,
  AddUserRequest,
  ErrorResponse,
  GetMeRequest,
  GetCommentsRequest,
  GetPostsRequest,
  GetUserRequest,
  SignInRequest,
  UpdateUserRequest,
  SearchRequest,
  UpdateFollowRequest
} from "./request";
import { initTables } from "./tables";

initTables();

export function handler(requestStr: string): string {
  let props: RequestProps = decode(requestStr);
  let response: string;

  if (props.action == "getPosts") {
    let request = new GetPostsRequest(props.token, props.userId);
    response = request.respond();

    request.clean();
    memory.free(changetype<usize>(request));
  } else if (props.action == "addPost") {
    let request = new AddPostRequest(
      props.postId,
      props.userId,
      props.content,
      props.timestamp,
      props.hashtags
    );
    response = request.respond();

    request.clean();
    memory.free(changetype<usize>(request));
  } else if (props.action == "getUser") {
    let request = new GetUserRequest(props.userId);
    response = request.respond();

    request.clean();
    memory.free(changetype<usize>(request));
  } else if (props.action == "addUser") {
    log("--- [HANDLER} RECIEVED AN ADDUSER REQUEST  --- ");
    let request = new AddUserRequest(
      props.userId,
      props.name,
      props.email,
      props.password,
      props.presentation
    );
    response = request.respond();

    request.clean();
    memory.free(changetype<usize>(request));
  } else if (props.action == "updateUser") {
    log("[Handler] Trying to update user");
    log(props.avatar);
    let request = new UpdateUserRequest(
      props.userId,
      props.token,
      props.presentation,
      props.password,
      props.avatar
    );
    response = request.respond();

    request.clean();
    memory.free(changetype<usize>(request));
  } else if (props.action == "getMe") {
    let request = new GetMeRequest(props.token);
    response = request.respond();

    request.clean();
    memory.free(changetype<usize>(request));
  } else if (props.action == "signIn") {
    let request = new SignInRequest(props.email, props.password);
    response = request.respond();

    request.clean();
    memory.free(changetype<usize>(request));
  } else if (props.action == "addComment") {
    let request = new AddCommentRequest(
      props.commentId,
      props.postId,
      props.userId,
      props.content,
      props.timestamp
    );
    response = request.respond();

    request.clean();
    memory.free(changetype<usize>(request));
  } else if (props.action == "getComments") {
    let request = new GetCommentsRequest(props.postId, props.token);
    response = request.respond();

    request.clean();
    memory.free(changetype<usize>(request));
  } else if (props.action == "search") {
    let request = new SearchRequest(props.token, props.method, props.target);
    response = request.respond();

    request.clean();
    memory.free(changetype<usize>(request));
  } else if (props.action == "updateFollow") {
    log("[HANDLER] updateFollow");
    log("targetUserId");
    log(props.targetUserId);

    let request = new UpdateFollowRequest(
      props.token,
      props.userId,
      props.targetUserId,
      props.following
    );
    response = request.respond();

    request.clean();
    memory.free(changetype<usize>(request));
  } else {
    let error = new ErrorResponse(
      404,
      "Unknown or invalid request " + props.action,
      "unknown"
    );
    response = error.serialize();
    memory.free(changetype<usize>(error));
  }

  props.clean();

  return response;
}
