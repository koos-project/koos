/*
 *
 *   __  ___   ______     ______        _______.
 *  |  |/  /  /  __  \   /  __  \      /       |
 *  |  '  /  |  |  |  | |  |  |  |    |   (----`
 *  |    <   |  |  |  | |  |  |  |     \   \
 *  |  .  \  |  `--'  | |  `--'  | .----)   |
 *  |__|\__\  \______/   \______/  |_______/
 *
 **/
import { log } from "../node_modules/assemblyscript-sdk/assembly/logger";

/*
 * This function have been integrated after
 * realizing the JSON function had a bug.
 *
 * TODO: Refactor it to parse JSON or YAML
 *
 */
export function decode(str: string): RequestProps {
  let requestProps = new RequestProps();
  /*
   * TODO: split using newline. We tried,
   * but it's not working for some reasons
   */
  let lines = str.trim().split("#$#");
  let action = lines[0];
  requestProps.setString("action", action);
  if (action == "signIn") {
    requestProps.setString("email", lines[1]);
    requestProps.setString("password", lines[2]);
  } else if (action == "getMe") {
    requestProps.setString("token", lines[1]);
  } else if (action == "addUser") {
    requestProps.setString("userId", lines[1]);
    requestProps.setString("name", lines[2]);
    requestProps.setString("email", lines[3]);
    requestProps.setString("password", lines[4]);
    requestProps.setString("presentation", lines[5]);
  } else if (action == "getPosts") {
    requestProps.setString("token", lines[1]);
  } else if (action == "addPost") {
    requestProps.setString("token", lines[1]);
    requestProps.setString("postId", lines[2]);
    requestProps.setString("userId", lines[3]);
    requestProps.setString("content", lines[4]);
    requestProps.setString("timestamp", lines[5]);
    requestProps.setString("hashtags", lines[6]);
  } else if (action == "getUser") {
    requestProps.setString("token", lines[1]);
    requestProps.setString("userId", lines[2]);
  } else if (action == "updateUser") {
    log("[decode]");
    log("userId");
    log(lines[1]);
    log("token");
    log(lines[2]);
    log(lines[3]);
    log(lines[4]);
    requestProps.setString("userId", lines[1]);
    requestProps.setString("token", lines[2]);
    requestProps.setString(lines[3], lines[4]);
  } else if (action == "addComment") {
    requestProps.setString("commentId", lines[1]);
    requestProps.setString("token", lines[2]);
    requestProps.setString("postId", lines[3]);
    requestProps.setString("userId", lines[4]);
    requestProps.setString("content", lines[5]);
    requestProps.setString("timestamp", lines[6]);
  } else if (action == "getComments") {
    requestProps.setString("postId", lines[1]);
    requestProps.setString("token", lines[2]);
  } else if (action == "search") {
    requestProps.setString("token", lines[1]);
    requestProps.setString("method", lines[2]);
    // Can be hashtag, post or user
    requestProps.setString("target", lines[3]);
  } else if (action == "updateFollow") {
    requestProps.setString("token", lines[1]);
    requestProps.setString("userId", lines[2]);
    requestProps.setString("targetUserId", lines[3]);
    requestProps.setString("following", lines[4]);
  }

  return requestProps;
}

export class RequestProps {
  public action: string;
  public avatar: string;
  public commentId: string;
  public content: string;
  public email: string;
  public hashtag: string;
  public hashtags: string;
  public limit: u32;
  public name: string;
  public password: string;
  public postId: string;
  public presentation: string;
  public skip: u32;
  public timestamp: u64;
  public token: string = "no-token";
  public userId: string;
  public method: string;
  public target: string;
  public targetUserId: string;
  public following: string;

  clean(): void {
    memory.free(changetype<usize>(this.action));
    memory.free(changetype<usize>(this.avatar));
    memory.free(changetype<usize>(this.content));
    memory.free(changetype<usize>(this.email));
    memory.free(changetype<usize>(this.hashtag));
    memory.free(changetype<usize>(this.hashtags));
    memory.free(changetype<usize>(this.limit));
    memory.free(changetype<usize>(this.name));
    memory.free(changetype<usize>(this.password));
    memory.free(changetype<usize>(this.presentation));
    memory.free(changetype<usize>(this.skip));
    memory.free(changetype<usize>(this.token));
    memory.free(changetype<usize>(this.method));
    memory.free(changetype<usize>(this.target));
    memory.free(changetype<usize>(this.targetUserId));
    memory.free(changetype<usize>(this.following));
  }

  setString(name: string, value: string): void {
    if (name == "action") {
      this.action = value;
    } else if (name == "name") {
      this.name = value;
    } else if (name == "content") {
      this.content = value;
    } else if (name == "email") {
      this.email = value;
    } else if (name == "presentation") {
      this.presentation = value;
    } else if (name == "password") {
      this.password = value;
    } else if (name == "token") {
      this.token = value;
    } else if (name == "hashtags") {
      this.hashtags = value;
    } else if (name == "hashtag") {
      this.hashtag = value;
    } else if (name == "postId") {
      this.postId = value;
    } else if (name == "userId") {
      this.userId = value;
    } else if (name == "commentId") {
      this.commentId = value;
    } else if (name == "method") {
      this.method = value;
    } else if (name == "target") {
      this.target = value;
    } else if (name == "avatar") {
      this.avatar = value;
    } else if (name == "targetUserId") {
      this.targetUserId = value;
    } else if (name == "following") {
      this.following = value;
    }
  }

  setInteger(name: string, value: i64): void {
    if (name == "skip") {
      this.skip = value as u32;
    } else if (name == "limit") {
      this.limit = value as u32;
    } else if (name == "timestamp") {
      this.time = value as u64;
    }
  }
}
