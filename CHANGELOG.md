## Changelog

This document will store the version history of Koos. When publishing a new version,
push it from `develop` to `master`, append the modification here 
and update the version of the the `develop` branch (in both server and client). 

# *Version 0.0.1*

* Implement sign up, sign in
* Implement posting and comments
* Implement profiles and update
* Implement avatars (roughly)

# *Version 0.0.2*

* Implement following features #27
* Implement search #23 (plain text only)
* Nasty fix for memory bug