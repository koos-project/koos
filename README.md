The main website is available at http://koos.network

![Intro](https://i.imgur.com/NRk4KGe.png)

Koos is a decentralised network project based on Fluence and Arweave.  It will
allow you to communicate, share and trade directly to your friends and member
of your local communities, with the convenience and speed of modern technology,
without the costs associated with a centralised system. 

Motivation
----------

Born in the early nineties, the worldwide web came a long way from an originally
decentralised technology to a technological landcape dominated by all-powerful
companies. The internet itself, ideal made true by a bizarre aliance between 
ex-hippies, universities and armies, always have been designed for peer-to-peer
communication and resilience. 

If it revolutionised the way humanity is processing informations, it
is subject to a certain kind of pendulum momentum from a distributed network to
a much more privately owned one. After [a piracy movement in the 2000s][1], that
changed the way we consume culture and arts, the rise of social networks and
marketplace created a huge amount of concentrated wealth. This movement is
currently curbing.

Scandals, such as [Cambridge Analytica][2], [PRISM][3], have triggered some people
to call for a [more open, decentralised web][4]. This is more and more made possible
by technologies, such as blockchains and permawebs that allow people and machine
to interact and trade in a [trustless environement][5]. The financial system was
the first to be impacted by those technologies, that allow to get rid of middlemen.
For the rest of the web, the work is still to be done.

The project
-----------

Koos have been built on the top of Fluence, a decentralized cloud solution. 
It is hosted on IPFS, the Interplanetary File System. As such, anybody can 
participate in the network and run a copy of Koos. It works in a purely 
peer-to-peer manner!

It is entirely open-source and is protected by the GNU General Public Licence v3.

About us
--------

Koos is a product of Chatnoir Digital. We are a worker-owned coop and a 
Decentralised Autonomous Organisation (DAO) dedicated to the finding socially 
empowering applications to open-source, decentralized, blockchain-based 
technologies. Don't hesitate to hit us up at our riot.im. Also, feel free to 
contribute to the project! We're looking for people to help us. 

[1]: https://en.wikipedia.org/wiki/Copyright_infringement#Noncommercial_file_sharing
[2]: https://en.wikipedia.org/wiki/Cambridge_Analytica
[3]: https://en.wikipedia.org/wiki/Prism
[4]: https://techcrunch.com/2018/10/09/tim-berners-lee-is-on-a-mission-to-decentralize-the-web/
[5]: https://medium.com/@preethikasireddy/eli5-what-do-we-mean-by-blockchains-are-trustless-aa420635d5f6
[logo]: https://files.slack.com/files-pri/TLYPK6HGQ-FM4QABYR5/logo.png
